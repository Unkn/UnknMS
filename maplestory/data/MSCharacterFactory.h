/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSCHARACTERFACTORY_H
#define MSCHARACTERFACTORY_HJ

#include <memory>

#include "MSCharacter.h"

namespace MSCharacterFactory
{
    struct NewCharArgs
    {
        std::string_view name_;
        uint32_t face_;
        uint32_t hair_;
        uint32_t hair_color_;
        // TODO 8 bit DB entry as it was for TitanSM... should this and hair
        // color be uint8_t?
        uint32_t skin_;
        uint32_t coat_;
        uint32_t pants_;
        uint32_t shoes_;
        uint32_t weapon_;
        MSGender gender_;

        uint8_t str_;
        uint8_t dex_;
        uint8_t int_;
        uint8_t luk_;
    };

    std::unique_ptr<MSCharacter> new_character(const NewCharArgs & args);
}

#endif

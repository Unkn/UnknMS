/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSCHARACTER_H
#define MSCHARACTER_H

#include <cstdint>
#include <string>
#include <vector>

#include "MSJob.h"
#include "MSEquipType.h"
#include "MSGender.h"

// TODO If it turns out this is useful elsewhere, pull it into its own file...
// maybe even move all this stuff into a structures dir :|
class MSEquip
{
    public:
        MSEquip();
        MSEquip(const MSEquip & obj);
        virtual ~MSEquip();

        // TODO Remove
        MSEquip(
                uint32_t dbid,
                MSEquipType type,
                uint32_t id) :
            dbid_(dbid),
            type_(type),
            id_(id)
        {}

        MSEquip & operator=(const MSEquip & rhs);

        // TODO id_
        uint32_t dbid_;
        MSEquipType type_;
        // TODO item_id_
        uint32_t id_;

        uint8_t slots_;
        uint8_t scrolls_;

        // TODO verify the size limits for all these
        uint8_t str_;
        uint8_t dex_;
        uint8_t int_;
        uint8_t luk_;

        uint8_t hp_;
        uint8_t mp_;

        uint8_t watk_;
        uint8_t matk_;
        uint8_t wdef_;
        uint8_t mdef_;

        uint8_t acc_;
        uint8_t avo_;
        uint8_t hand_; // TODO What is hand

        uint8_t speed_;
        uint8_t jump_;
};

class MSCharacter
{
    public:
        MSCharacter();
        MSCharacter(const MSCharacter & obj);
        virtual ~MSCharacter();

        MSCharacter & operator=(const MSCharacter & rhs);

        uint32_t dbid_;

        uint8_t level_;
        MSGender gender_;
        uint8_t skin_;
        uint8_t pos_;

        MSJob job_;
        uint16_t str_;
        uint16_t dex_;
        uint16_t int_;
        uint16_t luk_;
        uint16_t curHP_;
        uint16_t maxHP_;
        uint16_t curMP_;
        uint16_t maxMP_;
        uint16_t ap_;
        uint16_t sp_;
        uint16_t fame_;

        uint32_t face_;
        uint32_t hair_;
        uint32_t hair_color_;
        uint32_t exp_;
        uint32_t map_;

        // TODO I feel like this should go in some like inventory struct
        uint32_t mesos_;

        std::string name_;
        // TODO Make this UP<MSEquip>
        std::vector<MSEquip> equips_;
};

#endif

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSEQUIPTYPE_H
#define MSEQUIPTYPE_H

#include <cstdint>

enum class MSEquipType : uint8_t
{
    None        = 0x00,

    Hat         = 0x01,
    Accessory   = 0x02,
    // 3?
    // 4?
    Coat        = 0x05,
    LongCoat    = 0x05,
    Pants       = 0x06,
    Shoes       = 0x07,
    Glove       = 0x08,
    Cape        = 0x09,
    Shield      = 0x0A,
    Weapon      = 0x0B,
    Ring        = 0x0C,
};

#endif

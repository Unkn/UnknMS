/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSJOB_H
#define MSJOB_H

#include <cstdint>

enum class MSJob : uint16_t
{
    Beginner        = 0,

    Warrior         = 100,

    Fighter         = 110,
    Crusader        = 111,
    Hero            = 112,

    Page            = 120,
    WhiteKnight     = 121,
    Paladin         = 122,

    Spearman        = 130,
    DragonKnight    = 131,
    DarkKnight      = 132,

    Magician        = 200,

    FPWizard        = 210,
    FPMage          = 211,
    FPArchMage      = 212,

    ILWizard        = 220,
    ILMage          = 221,
    ILArchMage      = 222,

    Cleric          = 230,
    Priest          = 231,
    Bishop          = 232,

    Bowman          = 300,

    Hunter          = 310,
    Ranger          = 311,
    BowMaster       = 312,

    CrossbowMan     = 320,
    Sniper          = 321,
    CrossbowMaster  = 322,

    Thief           = 400,

    Assassin        = 410,
    Hermit          = 411,
    NightLord       = 412,

    Bandit          = 420,
    ChiefBandit     = 421,
    Shadower        = 422,

    GM              = 500,
    SuperGM         = 510,
};

#endif

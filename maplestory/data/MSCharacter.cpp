/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSCharacter.h"

MSEquip::MSEquip() :
    dbid_(0),
    type_(MSEquipType::None),
    id_(0),
    slots_(0),
    scrolls_(0),
    str_(0),
    dex_(0),
    int_(0),
    luk_(0),
    hp_(0),
    mp_(0),
    watk_(0),
    matk_(0),
    wdef_(0),
    mdef_(0),
    acc_(0),
    avo_(0),
    hand_(0),
    speed_(0),
    jump_(0)
{
}

MSEquip::MSEquip(const MSEquip & obj)
{
    *this = obj;
}

MSEquip::~MSEquip()
{
}

MSEquip & MSEquip::operator=(const MSEquip & rhs)
{
    if (this != &rhs)
    {
        // TODO Could throw all these into a struct and union with a byte array
        // for 1 line copies of all the primitives via memcpy :|
        dbid_ = rhs.dbid_;
        type_ = rhs.type_;
        id_ = rhs.id_;

        slots_ = rhs.slots_;
        scrolls_ = rhs.scrolls_;
        str_ = rhs.str_;
        dex_ = rhs.dex_;
        int_ = rhs.int_;
        luk_ = rhs.luk_;
        hp_ = rhs.hp_;
        mp_ = rhs.mp_;
        watk_ = rhs.watk_;
        matk_ = rhs.matk_;
        wdef_ = rhs.wdef_;
        mdef_ = rhs.mdef_;
        acc_ = rhs.acc_;
        avo_ = rhs.avo_;
        hand_ = rhs.hand_;
        speed_ = rhs.speed_;
        jump_ = rhs.jump_;
    }
    return *this;
}

MSCharacter::MSCharacter() :
    // NOTE: It appears that by how the client ignores a created character with
    // id 0, 0 may be an invalid id.
    dbid_(0),
    level_(0),
    gender_(MSGender::Male),
    skin_(0),
    pos_(0),
    job_(MSJob::Beginner),
    str_(0),
    dex_(0),
    int_(0),
    luk_(0),
    curHP_(0),
    maxHP_(0),
    curMP_(0),
    maxMP_(0),
    ap_(0),
    sp_(0),
    fame_(0),
    face_(0),
    hair_(0),
    hair_color_(0),
    exp_(0),
    map_(0),
    mesos_(0),
    name_(""),
    equips_()
{
}

MSCharacter::MSCharacter(const MSCharacter & obj)
{
    *this = obj;
}

MSCharacter::~MSCharacter()
{
}

MSCharacter & MSCharacter::operator=(const MSCharacter & rhs)
{
    if (this != &rhs)
    {
        dbid_ = rhs.dbid_;
        level_ = rhs.level_;
        gender_ = rhs.gender_;
        skin_ = rhs.skin_;
        pos_ = rhs.pos_;
        job_ = rhs.job_;
        str_ = rhs.str_;
        dex_ = rhs.dex_;
        int_ = rhs.int_;
        luk_ = rhs.luk_;
        curHP_ = rhs.curHP_;
        maxHP_ = rhs.maxHP_;
        curMP_ = rhs.curMP_;
        maxMP_ = rhs.maxMP_;
        ap_ = rhs.ap_;
        sp_ = rhs.sp_;
        fame_ = rhs.fame_;
        face_ = rhs.face_;
        hair_ = rhs.hair_;
        hair_color_ = rhs.hair_color_;
        exp_ = rhs.exp_;
        map_ = rhs.map_;
        mesos_ = rhs.mesos_;
        name_ = rhs.name_;
        equips_ = rhs.equips_;
    }

    return *this;
}

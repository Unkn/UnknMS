/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSCharacterFactory.h"

#include "Logger.h"

// Some macros to generate the simply switch body functions that are pretty
// repetitive. Knew it could be done but the following link definately helped
// figure it out. These don't need to be debugable otherwise I would have used
// some stl container with find or any_of or somethin.
//
//      https://stackoverflow.com/a/2124385
//
#define TRUE_IF_IN(in,...) switch (in) { TRUE_IF_IN_NARG(__VA_ARGS__) return true; default: return false; }
#define TRUE_IF_IN_NARG(...) TRUE_IF_IN_NARG_(__VA_ARGS__,TRUE_IF_IN_RSEQ_N())(__VA_ARGS__)
#define TRUE_IF_IN_NARG_(...) TRUE_IF_IN_ARG_N(__VA_ARGS__)
#define TRUE_IF_IN_ARG_N(_1, _2, _3, _4, N,...) N
#define TRUE_IF_IN_RSEQ_N() CASES4,CASES3,CASES2,CASES1,
#define CASES1(x) case x:
#define CASES2(x,y) CASES1(x) CASES1(y)
#define CASES3(x,...) CASES1(x) CASES2(__VA_ARGS__)
#define CASES4(x,...) CASES1(x) CASES3(__VA_ARGS__)

static inline bool valid_stats(uint8_t str, uint8_t dex, uint8_t intel, uint8_t luk);
static inline bool valid_stat(uint8_t stat);

static inline bool valid_weapon(uint32_t weapon);
static inline bool valid_hair_color(uint32_t hair_color);
static inline bool valid_skin(uint32_t skin);
static inline bool valid_shoes(uint32_t shoes);

static inline bool valid_style(MSGender gender, uint32_t face, uint32_t hair, uint32_t coat, uint32_t pants);

static inline bool valid_style_male(uint32_t face, uint32_t hair, uint32_t coat, uint32_t pants);
static inline bool valid_style_female(uint32_t face, uint32_t hair, uint32_t coat, uint32_t pants);
static inline bool valid_face_male(uint32_t face);
static inline bool valid_face_female(uint32_t face);
static inline bool valid_hair_male(uint32_t hair);
static inline bool valid_hair_female(uint32_t hair);
static inline bool valid_coat_male(uint32_t coat);
static inline bool valid_coat_female(uint32_t coat);
static inline bool valid_pants_male(uint32_t pants);
static inline bool valid_pants_female(uint32_t pants);

std::unique_ptr<MSCharacter> MSCharacterFactory::new_character(
        const NewCharArgs & args)
{
    std::unique_ptr<MSCharacter> ret;
    if (!valid_stats(args.str_, args.dex_, args.int_, args.luk_))
    {
        // TODO Let this get through but do a troll creation with like all 0
        // stats and jailed with some means of escape. Maybe have it as a
        // config or compile option.
        LOG_WARN("Invalid stat config for new char ({:d},{:d},{:d},{:d})",
                args.str_, args.dex_, args.int_, args.luk_);
    }
    else if (!(valid_weapon(args.weapon_) &&
               valid_hair_color(args.hair_color_) &&
               valid_skin(args.skin_) &&
               valid_shoes(args.shoes_) &&
               valid_style(args.gender_, args.face_, args.hair_, args.coat_, args.pants_)))
    {
        // TODO See above.
        LOG_WARN("Invalid style for new char");
    }
    else
    {
        ret = std::make_unique<MSCharacter>();

        ret->job_ = MSJob::Beginner;
        ret->level_ = 1;
        ret->curHP_ = 50;
        ret->maxHP_ = 50;
        ret->curMP_ = 50;
        ret->maxMP_ = 50;
        ret->ap_ = 0;
        ret->sp_ = 0;
        ret->fame_ = 0;
        ret->exp_ = 0;
        ret->map_ = 0; // TODO Which map?

        ret->name_ = args.name_;
        ret->face_ = args.face_;
        ret->hair_ = args.hair_;
        ret->hair_color_ = args.hair_color_;
        ret->skin_ = args.skin_;
        ret->gender_ = args.gender_;

        ret->str_ = args.str_;
        ret->dex_ = args.dex_;
        ret->int_ = args.int_;
        ret->luk_ = args.luk_;

        // TODO Method to get default and rand drop stats for equip
        auto & e = ret->equips_;
        e.resize(4);

        e[0].type_ = MSEquipType::Coat;
        e[1].type_ = MSEquipType::Pants;
        e[2].type_ = MSEquipType::Shoes;
        e[3].type_ = MSEquipType::Weapon;

        e[0].id_ = args.coat_;
        e[1].id_ = args.pants_;
        e[2].id_ = args.shoes_;
        e[3].id_ = args.weapon_;
    }

    return ret;
}

bool valid_stats(uint8_t str, uint8_t dex, uint8_t intel, uint8_t luk)
{
    // Could loop on the input packet but this isnt performance critical or anything
    return valid_stat(str) &&
           valid_stat(dex) &&
           valid_stat(intel) &&
           valid_stat(luk) &&
           (str + dex + intel + luk) == 25;
}

bool valid_stat(uint8_t stat)
{
    return 4 <= stat && stat <= 13;
}

bool valid_weapon(uint32_t weapon)
{
    TRUE_IF_IN(weapon,
        1312004, // Hand Axe
        1302000, // Sword
        1322005  // Wooden Club
        )
}

bool valid_style(
        MSGender gender,
        uint32_t face,
        uint32_t hair,
        uint32_t coat,
        uint32_t pants)
{
    bool ok = false;
    switch (gender)
    {
        case MSGender::Male:
            ok = valid_style_male(face, hair, coat, pants);
            break;
        case MSGender::Female:
            ok = valid_style_female(face, hair, coat, pants);
            break;
        default:
            LOG_WARN("Invalid gender for new char {:d}", static_cast<std::underlying_type<MSGender>::type>(gender));
            break;
    };
    return ok;
}

bool valid_style_male(
        uint32_t face,
        uint32_t hair,
        uint32_t coat,
        uint32_t pants)
{
    return valid_face_male(face) &&
        valid_hair_male(hair) &&
        valid_coat_male(coat) &&
        valid_pants_male(pants);
}

bool valid_style_female(
        uint32_t face,
        uint32_t hair,
        uint32_t coat,
        uint32_t pants)
{
    return valid_face_female(face) &&
        valid_hair_female(hair) &&
        valid_coat_female(coat) &&
        valid_pants_female(pants);
}

bool valid_face_male(uint32_t face)
{
    TRUE_IF_IN(face,
            0x4E20, // Male 1 (Black)
            0x4E21, // Male 2 (Black)
            0x4E22  // Leisure Look (Black)
            )
}

bool valid_face_female(uint32_t face)
{
    TRUE_IF_IN(face,
            0x5208, // Female 1 (Black)
            0x5209, // Female 2 (Black)
            0x520A  // Female 3 (Black)
            )
}

bool valid_hair_male(uint32_t hair)
{
    TRUE_IF_IN(hair,
            0x7544, // Disheveled Hair
            0x7530, // Toben Hair
            0x754E  // Buzzcut
            )
}

bool valid_hair_female(uint32_t hair)
{
    TRUE_IF_IN(hair,
            0x7918, // Cutie Hair
            0x7940, // Down-and-Straight Hair
            0x794A  // Bobbed hair
            )
}

bool valid_coat_male(uint32_t coat)
{
    TRUE_IF_IN(coat,
            0x0FDE82, // White Undershirt
            0x0FDE86, // Undershirt
            0x0FDE8A  // Grey T-Shirt
            )
}

bool valid_coat_female(uint32_t coat)
{
    TRUE_IF_IN(coat,
            0x0FE26A, // White Tubetop
            0x0FE26E, // Yellow T-Shirt
            0x0FE272, // Green T-Shirt
            0x0FE273  // Red-Striped Top
            )
}

bool valid_pants_male(uint32_t pants)
{
    TRUE_IF_IN(pants,
            0x102CA2, // Blue Jean Shorts
            0x102CA6  // Brown Cotton Shorts
            )
}

bool valid_pants_female(uint32_t pants)
{
    TRUE_IF_IN(pants,
            0x10308A, // Red Miniskirt
            0x103090  // Indigo Miniskirt
            )
}

bool valid_hair_color(uint32_t hair_color)
{
    TRUE_IF_IN(hair_color,
            0, // Black
            2, // Orange
            3, // Blond
            7  // Brown
            )
}

bool valid_skin(uint32_t skin)
{
    TRUE_IF_IN(skin,
            0, // Light
            1, // Tanned
            2, // Dark
            3  // Pale
            )
}

bool valid_shoes(uint32_t shoes)
{
    TRUE_IF_IN(shoes,
            0x105B81, // Red Rubber Boots
            0x105B85, // Leather Sandles
            0x105BA5, // Yellow Rubber Boots
            0x105BA6  // Blue Rubber Boots
            )
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSDBAccount.h"

#include <cassert>

#include "Logger.h"
#include "MSDBRHAccount.h"

MSDBAccount::MSDBAccount(
        const std::shared_ptr<MySQLConnection> & conn) :
    conn_(conn)
{
}

MSDBAccount::~MSDBAccount()
{
    // Pineapples
}

std::unique_ptr<MSAccount> MSDBAccount::get(std::string_view name)
{
    std::unique_ptr<MSAccount> ret;

    std::string stmt = "SELECT id, user_type, pass, salt, pin FROM accounts WHERE name=?";
    static_assert(ACCOUNT_RH_BINDINGS == 5, "Result binding count mismatch");

    // TODO Need to make something to have an even lazier bind. Problem is that
    // the MYSQL_BIND's need to be contiguous
    MYSQL_BIND bind_name;
    unsigned long name_len = name.size();
    bindIt(bind_name, name, name_len, name_len);
    MSDBRHAccount result;

    conn_->beginTransaction();

    if (!conn_->query(stmt, &bind_name, 1, result))
        LOG_WARN("Failed to query account '{}'", name);
    else if (!(ret = std::move(result.getAccount())))
        LOG_WARN("No account exists with name '{}'", name);
    else
        ret->name_ = name;

    conn_->endTransaction();

    return ret;
}

bool MSDBAccount::update_last_access(const MSAccount & account __attribute__ ((unused)))
{
    // TODO
    return false;
}

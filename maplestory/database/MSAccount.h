/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSACCOUNT_H
#define MSACCOUNT_H

#include <memory>

#include "Zeroize.h"

#define MS_ACCOUNT_SALT_LEN 32
// Length of SHA256 hash
#define MS_ACCOUNT_HMAC_LEN 32
#define MS_ACCOUNT_PIN_LEN  4

struct MSAuthData
{
    MSAuthData()
    {
        memset(salt_, 0, sizeof(salt_));
        memset(hmac_, 0, sizeof(hmac_));
        memset(pin_, 0, sizeof(pin_));
    }

    ~MSAuthData()
    {
        // It isnt THAT sensitive... but why not. There isn't any reason to
        // zeroize the salt or hmac though since they are not sensitive at all.
        zeroize(pin_, sizeof(pin_));
    }

    uint8_t salt_[MS_ACCOUNT_SALT_LEN];
    uint8_t hmac_[MS_ACCOUNT_HMAC_LEN];
    char pin_[MS_ACCOUNT_PIN_LEN];
};

struct MSAccount
{
    uint8_t type_;
    int32_t id_;
    std::unique_ptr<MSAuthData> auth_;
    std::string name_;
};

#endif

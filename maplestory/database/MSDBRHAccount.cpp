/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSDBRHAccount.h"

MSDBRHAccount::MSDBRHAccount() :
    processed_(false),
    account_(),
    lazy_()
{
    bindNewData();
}

MSDBRHAccount::~MSDBRHAccount()
{
}

MYSQL_BIND * MSDBRHAccount::getBindings()
{
    return lazy_.bindings_;
}

unsigned int MSDBRHAccount::getBindingCount() const
{
    return ACCOUNT_RH_BINDINGS;
}

bool MSDBRHAccount::processRow()
{
    if (processed_)
        return false;
    processed_ = true;
    return true;
}

std::unique_ptr<MSAccount> MSDBRHAccount::getAccount()
{
    std::unique_ptr<MSAccount> ret;

    if (processed_ && account_)
    {
        ret = std::move(account_);
        account_.reset();
    }

    return ret;
}

void MSDBRHAccount::bindNewData()
{
    account_ = std::make_unique<MSAccount>();
    account_->auth_ = std::make_unique<MSAuthData>();
    auto auth = account_->auth_.get();

    lazy_.bind(account_->id_);
    lazy_.bind(account_->type_);
    lazy_.bind(auth->hmac_, MS_ACCOUNT_HMAC_LEN);
    lazy_.bind(auth->salt_, MS_ACCOUNT_SALT_LEN);
    lazy_.bind(auth->pin_, MS_ACCOUNT_PIN_LEN);

    processed_ = false;
}

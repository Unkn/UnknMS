/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSDBRHEQUIPS_H
#define MSDBRHEQUIPS_H

#include <vector>

#include "MySQLResultHandler.h"
#include "MySQLLazyBind.h"
#include "MSCharacter.h"

#define EQUIPS_RH_BINDINGS 20

class MSDBRHEquips : public MySQLResultHandler
{
    public:
        MSDBRHEquips();
        virtual ~MSDBRHEquips();

        virtual MYSQL_BIND * getBindings();
        virtual unsigned int getBindingCount() const;
        virtual bool processRow();

        std::vector<MSEquip> equips_;

    private:
        MSEquip bind_equip_;
        LazyResultBind<EQUIPS_RH_BINDINGS> bindings_;
};

#endif

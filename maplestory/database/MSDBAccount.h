/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSDBACCOUNT_H
#define MSDBACCOUNT_H

#include <memory>
#include <string_view>

#include "MySQLConnection.h"

#include "MSAccount.h"

// TODO Rename MSDBAccounts?
class MSDBAccount
{
    public:
        MSDBAccount(const std::shared_ptr<MySQLConnection> & conn);
        virtual ~MSDBAccount();

        virtual std::unique_ptr<MSAccount> get(std::string_view name);
        virtual bool update_last_access(const MSAccount & account);

    private:
        std::shared_ptr<MySQLConnection> conn_;
};

#endif

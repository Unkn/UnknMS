/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSDBCHARACTER_H
#define MSDBCHARACTER_H

#include <string_view>
#include <memory>

#include "MySQLConnection.h"
#include "MSCharacter.h"

class MSDBCharacter
{
    public:
        MSDBCharacter(const std::shared_ptr<MySQLConnection> & conn);
        virtual ~MSDBCharacter();

        virtual bool reserve_name(uint32_t account_id, std::string_view name);
        // TODO Be sure to release a reserved name on client disconnect
        virtual void release_unused_names(uint32_t account_id);
        virtual void clear_all_unused_names();
        virtual bool create_character(uint32_t account, MSCharacter & character);
        virtual bool get_characters(uint32_t account_id, std::vector<std::unique_ptr<MSCharacter> > & characters);

    private:
        std::shared_ptr<MySQLConnection> conn_;

        bool insert_new_character(MSCharacter & character);
        bool insert_new_equips(MSCharacter & character);
        bool insert_reserved_name(uint32_t account_id, std::string_view name);
        bool verify_name_is_unused(std::string_view name);
        bool delete_unused_names(uint32_t account_id);
        bool update_reserved_name_with_id(uint32_t account_id, const MSCharacter & character);
        bool select_characters(uint32_t account_id, std::vector<std::unique_ptr<MSCharacter> > & characters);
        bool select_equipped_items(MSCharacter & character);
};

#endif

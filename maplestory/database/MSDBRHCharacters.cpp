/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSDBRHCharacters.h"

#include "Types.h"

MSDBRHCharacters::MSDBRHCharacters()
{
    auto & c = bind_char_;
    auto & b = bindings_;

    b.bind(c.dbid_);
    b.bind(name_, sizeof(name_));
    b.bind(c.level_);
    b.bind(underlying(c.job_));

    b.bind(c.str_);
    b.bind(c.dex_);
    b.bind(c.int_);
    b.bind(c.luk_);

    b.bind(c.curHP_);
    b.bind(c.maxHP_);
    b.bind(c.curMP_);
    b.bind(c.maxMP_);

    b.bind(c.ap_);
    b.bind(c.sp_);
    b.bind(c.exp_);
    b.bind(c.mesos_);
    b.bind(c.fame_);

    b.bind(underlying(c.gender_));
    b.bind(c.skin_);
    b.bind(c.face_);
    b.bind(c.hair_);
    b.bind(c.hair_color_);

    b.bind(c.pos_);
    b.bind(c.map_);
}

MSDBRHCharacters::~MSDBRHCharacters()
{
}

MYSQL_BIND * MSDBRHCharacters::getBindings()
{
    return bindings_.bindings();
}

unsigned int MSDBRHCharacters::getBindingCount() const
{
    return bindings_.count();
}

bool MSDBRHCharacters::processRow()
{
    const int NAME_INDEX = 1;

    characters_.emplace_back(std::make_unique<MSCharacter>(bind_char_));
    characters_.back()->name_.assign(name_, bindings_.length_[NAME_INDEX]);

    return true;
}

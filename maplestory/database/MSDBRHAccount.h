/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSDBRHACCOUNT_H
#define MSDBRHACCOUNT_H

#include <memory>

#include "MySQLResultHandler.h"
#include "MSAccount.h"
#include "MySQLLazyBind.h"

#define ACCOUNT_RH_BINDINGS 5

class MSDBRHAccount : public MySQLResultHandler
{
    public:
        MSDBRHAccount();
        virtual ~MSDBRHAccount();

        virtual MYSQL_BIND * getBindings();
        virtual unsigned int getBindingCount() const;
        virtual bool processRow();

        std::unique_ptr<MSAccount> getAccount();

    private:
        bool processed_;
        std::unique_ptr<MSAccount> account_;
        LazyResultBind<ACCOUNT_RH_BINDINGS> lazy_;

        void bindNewData();
};

#endif

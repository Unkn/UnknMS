/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSDBRHCHARACTERS_H
#define MSDBRHCHARACTERS_H

#include <memory>
#include <vector>

#include "MySQLResultHandler.h"
#include "MySQLLazyBind.h"
#include "MSCharacter.h"

#define MS_MAX_CHAR_NAME 12
#define CHARACTERS_RH_BINDINGS 24

class MSDBRHCharacters : public MySQLResultHandler
{
    public:
        MSDBRHCharacters();
        virtual ~MSDBRHCharacters();

        virtual MYSQL_BIND * getBindings();
        virtual unsigned int getBindingCount() const;
        virtual bool processRow();

        std::vector<std::unique_ptr<MSCharacter> > characters_;

    private:
        char name_[MS_MAX_CHAR_NAME];
        MSCharacter bind_char_;
        LazyResultBind<CHARACTERS_RH_BINDINGS> bindings_;
};

#endif

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSDBCharacter.h"

#include "Logger.h"
#include "Types.h"
#include "MySQLLazyBind.h"
#include "MySQLRHSelect1.h"
#include "MSDBRHCharacters.h"
#include "MSDBRHEquips.h"

// TODO The character data access should be in a separate database for the
// world the character belongs to so it can reside on a separate server.
// However the login server should maintain a table of occupied names along
// with the table of temporarily reserved names.

MSDBCharacter::MSDBCharacter(
        const std::shared_ptr<MySQLConnection> & conn) :
    conn_(conn)
{
}

MSDBCharacter::~MSDBCharacter()
{
    // Pineapples
}

bool MSDBCharacter::reserve_name(
        uint32_t account_id,
        std::string_view name)
{
    bool ret = insert_reserved_name(account_id, name);
    if (ret)
        LOG_NOTICE("Name '{}' has been reserved for account {:d}", name, account_id);
    return ret;
}

void MSDBCharacter::release_unused_names(
        uint32_t account_id)
{
    if (!delete_unused_names(account_id))
        LOG_WARN("Error releasing names marked as reserved for account {:d}", account_id);
}

void MSDBCharacter::clear_all_unused_names()
{
    std::string stmt = "DELETE FROM character_names WHERE character_id IS NULL";
    if (!conn_->exec(stmt))
        LOG_ERROR("Failed to clear all reserved names");
}

bool MSDBCharacter::create_character(
        uint32_t account,
        MSCharacter & character)
{
    bool ok = false;

    conn_->beginTransaction();
    ok = insert_new_character(character) &&
         insert_new_equips(character) &&
         update_reserved_name_with_id(account, character);
    conn_->endTransaction(ok);

    return ok;
}

bool MSDBCharacter::get_characters(
        uint32_t account_id,
        std::vector<std::unique_ptr<MSCharacter> > & characters)
{
    bool ok = true;

    conn_->beginTransaction();
    if (!select_characters(account_id, characters))
        ; // TODO log error
    else
    {
        for (auto & c : characters)
        {
            if (!select_equipped_items(*c))
            {
                // TODO Log error
                ok = false;
                break;
            }
        }
    }
    conn_->endTransaction(ok);

    return ok;
}

bool MSDBCharacter::insert_new_character(
        MSCharacter & c)
{
    bool ok = true;

    std::string stmt =
        "INSERT INTO characters ("
            "level, "       // 1
            "job, "         // 2

            "str, "         // 3
            "dex, "         // 4
            "intel, "       // 5
            "luk, "         // 6

            "cur_hp, "      // 7
            "max_hp, "      // 8
            "cur_mp, "      // 9
            "max_mp, "      // 10

            "gender, "      // 11
            "skin, "        // 12
            "face, "        // 13
            "hair, "        // 14
            "hair_color, "  // 15

            "pos, "         // 16
            "map) "         // 17
        //      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
        "VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    // We dont want this on the stack. It's by no means small.
    auto pb = std::make_unique<LazyQueryBind<17> >();
    auto & b = *pb;

    b.bind(c.level_);
    b.bind(underlying(c.job_));

    b.bind(c.str_);
    b.bind(c.dex_);
    b.bind(c.int_);
    b.bind(c.luk_);

    b.bind(c.curHP_);
    b.bind(c.maxHP_);
    b.bind(c.curMP_);
    b.bind(c.maxMP_);

    b.bind(underlying(c.gender_));
    b.bind(c.skin_);
    b.bind(c.face_);
    b.bind(c.hair_);
    b.bind(c.hair_color_);

    b.bind(c.pos_);
    b.bind(c.map_);

    if (!conn_->exec(stmt, b.bindings_, b.count()))
    {
        LOG_WARN("Failed to insert new character into database");
        ok = false;
    }
    else
    {
        c.dbid_ = mysql_insert_id(&conn_->mysql_);
        LOG_INFO("Added new character '{}' to db with id {:d}", c.name_, c.dbid_);
        // TODO just set everything
    }

    return ok;
}

bool MSDBCharacter::insert_new_equips(
        MSCharacter & c)
{
    bool ok = true;
    std::string stmt = "INSERT INTO equips (character_id, type, item_id) VALUE (?, ?, ?)";
    auto pb = std::make_unique<LazyQueryBind<3> >();
    auto & b = *pb;
    MSEquip eb;

    b.bind(c.dbid_);
    b.bind(underlying(eb.type_));
    b.bind(eb.id_);

    auto prep = conn_->prepare(stmt, b);
    if (!prep)
    {
        LOG_DEBUG("Failed to prepare statement for adding character equips to db");
        ok = false;
    }
    else
    {
        for (auto & e : c.equips_)
        {
            eb = e;
            if (!conn_->exec(prep))
            {
                LOG_ERROR("Failed to add new equip to db");
                ok = false;
                break;
            }
            else
            {
                e.dbid_ = mysql_insert_id(&conn_->mysql_);
                LOG_INFO("Added new item {:d} ({:d}) for character {:d} to db", e.id_, e.dbid_, c.dbid_);
                // TODO Load remaining from db
            }
        }
    }

    return ok;
}

bool MSDBCharacter::insert_reserved_name(uint32_t account_id, std::string_view name)
{
    bool ret = false;
    std::string stmt = "INSERT INTO character_names (account_id, name) VALUE (?, ?)";
    auto pb = std::make_unique<LazyQueryBind<2> >();

    pb->bind(account_id);
    // Faith MySQL will not fuck with it :|
    pb->bind(const_cast<char *>(name.data()), name.length());

    if (!conn_->exec(stmt, *pb))
        LOG_WARN("Failed to insert name '{}' into reserved named for account {:d}", account_id, name);
    else
        ret = true;

    return ret;
}

bool MSDBCharacter::verify_name_is_unused(
        std::string_view name)
{
    bool ret = false;
    std::string stmt = "SELECT 1 FROM characters_names WHERE name=? LIMIT 1";
    auto pb = std::make_unique<LazyQueryBind<1> >();
    MySQLRHSelect1 result;

    // Faith MySQL will not fuck with it :|
    pb->bind(const_cast<char *>(name.data()), name.length());

    if (!conn_->query(stmt, *pb, result))
        LOG_WARN("Failed to check if character name {} is used", name);
    else
        ret = !result.processed();

    return ret;
}

bool MSDBCharacter::delete_unused_names(
        uint32_t account_id)
{
    bool ok = true;
    std::string stmt = "DELETE FROM character_names WHERE account_id=? AND character_id IS NULL";
    auto pb = std::make_unique<LazyQueryBind<1> >();

    pb->bind(account_id);

    if (!conn_->exec(stmt, *pb))
    {
        LOG_ERROR("Failed to clear reserved names for account {:d}", account_id);
        ok = false;
    }

    return ok;
}

bool MSDBCharacter::update_reserved_name_with_id(
        uint32_t account_id,
        const MSCharacter & character)
{
    bool ok = false;
    std::string stmt = "UPDATE character_names SET character_id=? WHERE account_id=? AND name=?";
    auto pb = std::make_unique<LazyQueryBind<3> >();
    // Non-const copy so we dont have to const cast =/
    auto dbid = character.dbid_;
    auto name = character.name_;
    int affected_rows = 0;

    pb->bind(dbid);
    pb->bind(account_id);
    pb->bind(name.data(), name.size());

    if (!conn_->exec(stmt, *pb, &affected_rows))
    {
        LOG_ERROR("Failed to update character name entry '{}' for account {:d} with new character id {:d}",
                name, account_id, character.dbid_);
    }
    else if (affected_rows != 1)
    {
        LOG_ERROR("Invalid number of rows updated for new character name association '{}', {:d}, {:d}",
                name, account_id, character.dbid_);
    }
    else
    {
        LOG_INFO("Reserved name '{}' has been assigned to new character {:d} for account {:d}",
                name, character.dbid_, account_id);
        ok = true;
    }

    return ok;
}

bool MSDBCharacter::select_characters(
        uint32_t account_id,
        std::vector<std::unique_ptr<MSCharacter> > & characters)
{
    bool ok = false;
    std::string stmt =
        "SELECT "
            "characters.id, "
            "character_names.name, "
            "characters.level, "
            "characters.job, "

            "characters.str, "
            "characters.dex, "
            "characters.intel, "
            "characters.luk, "

            "characters.cur_hp, "
            "characters.max_hp, "
            "characters.cur_mp, "
            "characters.max_mp, "

            "characters.ap, "
            "characters.sp, "
            "characters.exp, "
            "characters.mesos, "
            "characters.fame, "

            "characters.gender, "
            "characters.skin, "
            "characters.face, "
            "characters.hair, "
            "characters.hair_color, "

            "characters.pos, "
            "characters.map "
        "FROM characters "
        "INNER JOIN character_names ON characters.id=character_names.character_id "
        "WHERE account_id=? "
        "ORDER BY characters.id ASC";
    auto b = std::make_unique<LazyQueryBind<1> >();
    auto rh = std::make_unique<MSDBRHCharacters>();

    b->bind(account_id);

    if (!conn_->query(stmt, *b, *rh))
        ; // TODO Logging
    else
    {
        ok = true;
        characters = std::move(rh->characters_);
    }

    return ok;
}

bool MSDBCharacter::select_equipped_items(
        MSCharacter & c)
{
    bool ok = false;
    // TODO Profile the sql statement preparing to see if its worth cacheing
    // the prepared statements
    std::string stmt =
        "SELECT "
            "id, "
            "item_id, "
            "type, "

            "slots, "
            "scrolls, "

            "str, "
            "dex, "
            "intel, "
            "luk, "

            "hp, "
            "mp, "

            "watk, "
            "matk, "
            "wdef, "
            "mdef, "

            "acc, "
            "avo, "
            "hand, "

            "speed, "
            "jump "
        "FROM equips "
        "WHERE character_id=?";
    auto b = std::make_unique<LazyQueryBind<1> >();
    auto rh = std::make_unique<MSDBRHEquips>();

    b->bind(c.dbid_);

    if (!conn_->query(stmt, *b, *rh))
        ; // TODO Logging
    else
    {
        ok = true;
        c.equips_ = std::move(rh->equips_);
    }

    return ok;
}

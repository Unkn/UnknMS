/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSDBRHEquips.h"

#include "Types.h"

MSDBRHEquips::MSDBRHEquips()
{
    auto & e = bind_equip_;
    auto & b = bindings_;

    b.bind(e.dbid_);
    b.bind(e.id_);
    b.bind(underlying(e.type_));

    b.bind(e.slots_);
    b.bind(e.scrolls_);

    b.bind(e.str_);
    b.bind(e.dex_);
    b.bind(e.int_);
    b.bind(e.luk_);

    b.bind(e.hp_);
    b.bind(e.mp_);

    b.bind(e.watk_);
    b.bind(e.matk_);
    b.bind(e.wdef_);
    b.bind(e.mdef_);

    b.bind(e.acc_);
    b.bind(e.avo_);
    b.bind(e.hand_);

    b.bind(e.speed_);
    b.bind(e.jump_);
}

MSDBRHEquips::~MSDBRHEquips()
{
}

MYSQL_BIND * MSDBRHEquips::getBindings()
{
    return bindings_.bindings();
}

unsigned int MSDBRHEquips::getBindingCount() const
{
    return bindings_.count();
}

bool MSDBRHEquips::processRow()
{
    equips_.emplace_back(MSEquip(bind_equip_));
    return true;
}

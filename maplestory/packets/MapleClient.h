/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MAPLECLIENT_H
#define MAPLECLIENT_H

#include <memory>
#include <vector>

#include "MSCrypt.h"
#include "Socket.h"
#include "buffer.hpp"
#include "MSAccount.h"
#include "MSCharacter.h"

// TODO Split up into login and world/channel client
class MapleClient
{
    public:
        MapleClient(std::unique_ptr<Socket> && socket);
        virtual ~MapleClient();

        virtual bool establishConnection();
        virtual bool readPacket(Buffer &buff, bool decrypt = true);
        virtual bool sendPacket(Buffer &buff, bool encrypt = true);

        int getConnectionID() const;
        bool should_disconnect() const;

        std::unique_ptr<const MSAccount> account_;
        // TODO This should be limited to login server. The channel servers
        // only need the current character.
        std::vector<std::unique_ptr<MSCharacter> > characters_;
        std::string reserved_name_;

    private:
        std::unique_ptr<Socket> socket_;
        bool should_disconnect_;
        uint8_t sendIV_[MSCrypt::IV_LENGTH];
        uint8_t recvIV_[MSCrypt::IV_LENGTH];
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MapleClient *) -> unique_ptr<MapleClient, default_delete<MapleClient> >;
}

#endif

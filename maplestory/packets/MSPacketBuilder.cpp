/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPacketBuilder.h"

MSPacketBuilder::MSPacketBuilder() :
    raw_()
{
}

MSPacketBuilder::MSPacketBuilder(const MSPacketBuilder & obj)
{
    *this = obj;
}

MSPacketBuilder::MSPacketBuilder(MSPacketBuilder && obj)
{
    *this = std::move(obj);
}

MSPacketBuilder::~MSPacketBuilder()
{
    // Pineapples
}

MSPacketBuilder & MSPacketBuilder::operator=(const MSPacketBuilder & rhs)
{
    if (this != &rhs)
        raw_ = rhs.raw_;
    return *this;
}

MSPacketBuilder & MSPacketBuilder::operator=(MSPacketBuilder && rhs)
{
    if (this != &rhs)
        raw_ = std::move(rhs.raw_);
    return *this;
}

Buffer & MSPacketBuilder::getRaw()
{
    return raw_;
}

const Buffer & MSPacketBuilder::getRaw() const
{
    return raw_;
}

template<>
MSPacketBuilder & MSPacketBuilder::add(const std::string_view & value)
{
    uint16_t len = value.size();
    add(len);
    add(value.data(), len);
    return *this;
}

template<>
MSPacketBuilder & MSPacketBuilder::add(const std::string & value)
{
    return add<std::string_view>(value);
}

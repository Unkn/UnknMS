/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "Logger.h"
#include "MSPPConnectGame.h"
#include "MSPacketHeaders.h"
#include "MSLoginClient.h"

MSPPConnectGame::MSPPConnectGame()
{
}

MSPPConnectGame::~MSPPConnectGame()
{
}

uint16_t MSPPConnectGame::getHeader() const
{
    return MSPacketHeaders::v55::CONNECT_GAME;
}

bool MSPPConnectGame::process(
        Buffer &buffIn,
        std::vector<Buffer> & outBuffs,
        MapleClient &client)
{
    uint16_t header;
    uint32_t charID;

    Packet p(buffIn);

    p.get(header);
    p.get(charID);

    // TODO Validate the char id against the client

    bool ret = getConnectionPacket(outBuffs, static_cast<MSLoginClient &>(client), charID);

    return ret;
}

bool MSPPConnectGame::getConnectionPacket(
        std::vector<Buffer> & outBuffs,
        const MSLoginClient & client,
        uint32_t charID) const
{
    MSPacketBuilder b;

    b.add<uint16_t>(0x04); // TODO Move to MSPacketHeaders

    // TODO TitanMS says 'IP' but im not sure if its true :|
    // Find out what this is for...
    b.add<uint16_t>(0);

    // TODO Need to dependency inject the channel object lookups OR use like a
    // weak_ptr in the client object for the channel as part of the
    // world/channel select
    auto ip = std::array<uint8_t, 4>{ 10, 50, 0, 1 };
    uint16_t port = 8888;
    b.add(ip.data(), ip.size());
    b.add(port);

    LOG_DEBUG("Telling client {:d} to connect to {:d}.{:d}.{:d}.{:d}:{:d}",
            client.getConnectionID(), ip[0], ip[1], ip[2], ip[3], port);

    b.add(charID);
    b.add<uint32_t>(0); // TODO Whats this?
    b.add<uint8_t>(0); // TODO Whats this?

    outBuffs.emplace_back(std::move(b.getRaw()));

    // TODO In case of failing to retrieve the channel server shit when we get
    // to that. See the TODO about IP and port above.
    return true;
}

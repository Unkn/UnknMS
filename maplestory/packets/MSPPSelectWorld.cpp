/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPPSelectWorld.h"

#include "Logger.h"
#include "Types.h"
#include "MSPacketHeaders.h"
#include "MSPacketBuilder.h"
#include "MSLoginClient.h"
#include "MSPBSelectedWorld.h"

MSPPSelectWorld::MSPPSelectWorld()
{
}

MSPPSelectWorld::~MSPPSelectWorld()
{
}

uint16_t MSPPSelectWorld::getHeader() const
{
    return MSPacketHeaders::v55::SELECT_WORLD;
}

bool MSPPSelectWorld::process(
        Buffer &buffIn,
        std::vector<Buffer> & outBuffs,
        MapleClient &client)
{
    // 13 00     00    00
    // header, world id, ???
    // TODO Whats the final byte?
    bool ret = false;
    uint8_t world; // TODO May want to associate this with the client
    auto & lc = static_cast<MSLoginClient &>(client);

    Packet p(buffIn, true);

    if (lc.loginStatus_ != MSLoginStatus::WorldSelect)
    {
        LOG_WARN("Client {:d} attempted to select world with login status {:d}",
                client.getConnectionID(), underlying(lc.loginStatus_));
    }
    else if (!p.get(world))
    {
        LOG_WARN("Failed to parse world select packet from client {:d}",
                client.getConnectionID());
    }
    else
    {
        ret = true;
        lc.loginStatus_ = MSLoginStatus::ChannelSelect;

        LOG_DEBUG("Client {:d} requested world {:d}", client.getConnectionID(), world);

        MSPBSelectedWorld b;
        outBuffs.emplace_back(std::move(b.getRaw()));
    }

    return ret;
}

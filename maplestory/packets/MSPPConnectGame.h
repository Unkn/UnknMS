/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPPCONNECTGAME_H
#define MSPPCONNECTGAME_H

#include "MSPacketProcessor.h"
#include "MSPacketBuilder.h"
#include "MSLoginClient.h"

class MSPPConnectGame : public MSPacketProcessor
{
    public:
        MSPPConnectGame();
        virtual ~MSPPConnectGame();

        virtual uint16_t getHeader() const;
        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);

    private:
        bool getConnectionPacket(std::vector<Buffer> & outBuffs, const MSLoginClient & client, uint32_t charID) const;
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MSPPConnectGame *) -> unique_ptr<MSPPConnectGame, default_delete<MSPPConnectGame> >;
}

#endif

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPPWorldSelect.h"
#include "MSPacketHeaders.h"

MSPPWorldSelect::MSPPWorldSelect()
{
}

MSPPWorldSelect::~MSPPWorldSelect()
{
}

uint16_t MSPPWorldSelect::getHeader() const
{
    return MSPacketHeaders::v55::WORLD_SELECT;
}

bool MSPPWorldSelect::process(
        Buffer &buffIn __atribute__ ((unused)),
        std::vector<Buffer> & outBuffs __atribute__ ((unused)),
        MapleClient &client __atribute__ ((unused)))
{
    // TODO Come back to this when we get here...
    return false;
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSWorld.h"

MSWorld::MSWorld(
        uint8_t id,
        uint8_t channels,
        uint8_t type,
        const std::string & name) :
    id_(id),
    channels_(channels),
    type_(type),
    name_(name)
{
}

MSWorld::~MSWorld()
{
}

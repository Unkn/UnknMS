/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "Logger.h"
#include "MSPPShowWorlds.h"
#include "MSPacketHeaders.h"
#include "MSLoginClient.h"
#include "MSPacketBuilder.h"
#include "MSPackets.h"

MSPPShowWorlds::MSPPShowWorlds(
        const std::shared_ptr<std::vector<MSWorld> > & worlds) :
    worlds_(worlds)
{
}

MSPPShowWorlds::~MSPPShowWorlds()
{
}

uint16_t MSPPShowWorlds::getHeader() const
{
    return MSPacketHeaders::v55::SHOW_WORLDS;
}

bool MSPPShowWorlds::process(
        // TODO Check if there is any data in here we can use
        Buffer &buffIn __attribute__ ((unused)),
        std::vector<Buffer> & outBuffs,
        MapleClient &client)
{
    bool ok = true;
    auto & loginClient = static_cast<MSLoginClient &>(client);

    if (loginClient.loginStatus_ != MSLoginStatus::LoggedIn)
    {
        ok = false;
        LOG_WARN("Recieved packet from client {:d} to show worlds when not logged in", loginClient.getConnectionID());
    }
    else
    {
        for (auto & world : *worlds_)
            addWorldPacket(world, outBuffs);
        addWorldListTerminatorPacket(outBuffs);

        loginClient.loginStatus_ = MSLoginStatus::WorldSelect;
    }

    return ok;
}

void MSPPShowWorlds::addWorldPacket(
        const MSWorld & world,
        std::vector<Buffer> & outBuffs)
{
    MSPacketBuilder p;

    p.add(MSPacketHeaders::v55::SHOW_WORLD_LIST);
    p.add(world.id_);

    uint16_t nameSize = world.name_.size();
    p.add(nameSize);
    p.add(world.name_.data(), nameSize);

    p.add(world.type_);

    // TODO Unknown sections...
    p.add((uint16_t)0);
    p.add((uint16_t)100);
    p.add((uint8_t)100);
    p.add((uint16_t)0);

    p.add(world.channels_);

    // TODO Why is the channel naming like this?
    std::string channelName = world.name_;
    channelName += "-0";
    uint16_t channelNameLen = channelName.size();
    for (uint16_t i = 0; i < world.channels_; ++i)
    {
        ++channelName.back();
        p.add(channelNameLen);
        p.add(channelName.data(), channelNameLen);
        // TODO What is this one for?
        p.add((uint32_t)0);
        p.add(world.id_);
        p.add(i);
    }

    outBuffs.emplace_back(std::move(p.getRaw()));
}

void MSPPShowWorlds::addWorldListTerminatorPacket(
        std::vector<Buffer> & outBuffs)
{
    MSPacketBuilder p;

    p.add(MSPacketHeaders::v55::SHOW_WORLD_LIST);
    p.add((uint8_t)0xFF); // List terminator

    outBuffs.emplace_back(std::move(p.getRaw()));
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPPCheckName.h"

#include "Logger.h"
#include "Types.h"
#include "MSPacketHeaders.h"
#include "MSPBCheckName.h"
#include "MSLoginClient.h"

MSPPCheckName::MSPPCheckName(
        const std::shared_ptr<MSDBCharacter> & msdb_char) :
    msdb_char_(msdb_char)
{
}

MSPPCheckName::~MSPPCheckName()
{
    // Pineapples
}

uint16_t MSPPCheckName::getHeader() const
{
    return MSPacketHeaders::v55::CHECK_NAME;
}

bool MSPPCheckName::process(
        Buffer &buffIn,
        std::vector<Buffer> & outBuffs,
        MapleClient & client)
{
    bool ok = false;
    std::string_view name;
    Packet p(buffIn, true);
    auto & lc = static_cast<MSLoginClient &>(client);

    if (lc.loginStatus_ != MSLoginStatus::CharacterSelect)
    {
        LOG_WARN("Client {:d} attempted to reserve character name with invalid login status {:d}",
                client.getConnectionID(), underlying(lc.loginStatus_));
    }
    else if (!p.get(name))
    {
        LOG_WARN("Failed to parse name from packet");
    }
    // TODO When name is marked as available do we want to reserve it until
    // created or canceled or connection dropped for the client?
    //
    // Do we really want to lock on the name check though... this wont scale
    // well across multiple servers that share character names. There is
    // probably an error response as the time of creation available.
    else
    {
        ok = true;
        bool reserved = msdb_char_->reserve_name(lc.account_->id_, name);

        if (reserved)
        {
            client.reserved_name_ = name;
            LOG_INFO("Client {:d} reserved name '{}' for character creation",
                    client.account_->id_, name);
        }

        auto builder = MSPBCheckName(reserved, name);
        outBuffs.emplace_back(std::move(builder.getRaw()));
    }

    return ok;
}

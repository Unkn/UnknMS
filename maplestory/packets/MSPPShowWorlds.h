/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPPSHOWWORLDS_H
#define MSPPSHOWWORLDS_H

#include "MSPacketProcessor.h"
#include "MSWorld.h"

class MSPPShowWorlds : public MSPacketProcessor
{
    public:
        MSPPShowWorlds() = delete;
        MSPPShowWorlds(const std::shared_ptr<std::vector<MSWorld> > & worlds);
        virtual ~MSPPShowWorlds();

        virtual uint16_t getHeader() const;
        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);

    private:
        // TODO We may just want to make this a list if we do something like
        // dynamic world add/remove
        std::shared_ptr<std::vector<MSWorld> > worlds_;

        void addWorldPacket(const MSWorld & world, std::vector<Buffer> & outBuffs);
        void addWorldListTerminatorPacket(std::vector<Buffer> & outBuffs);
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MSPPShowWorlds *) -> unique_ptr<MSPPShowWorlds, default_delete<MSPPShowWorlds> >;
}

#endif

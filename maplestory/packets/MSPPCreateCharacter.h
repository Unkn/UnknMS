/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPPCREATECHARACTER_H
#define MSPPCREATECHARACTER_H

#include <memory>

#include "MSPacketProcessor.h"
#include "MSCharacter.h"
#include "MSDBCharacter.h"

class MSPPCreateCharacter : public MSPacketProcessor
{
    public:
        MSPPCreateCharacter(const std::shared_ptr<MSDBCharacter> & msdb_char);
        virtual ~MSPPCreateCharacter();

        virtual uint16_t getHeader() const;
        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);

    private:
        std::shared_ptr<MSDBCharacter> msdb_char_;
};

#endif

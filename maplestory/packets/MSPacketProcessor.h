/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPACKETPROCESSOR_H
#define MSPACKETPROCESSOR_H

#include <string_view>
#include <vector>

#include "buffer.hpp"
#include "Logger.h"
#include "MapleClient.h"

struct PacketSegment
{
    uint8_t * data_;
    size_t len_;
};

// TODO Just a convenience idea...
// TODO structure overlay instead of 'Packet' object; do not forget to set
// alignment to 1 for them
struct Packet
{
    public:
        Packet(Buffer &buff, bool skip_header = false) :
            buff_(buff),
            pos_(skip_header ? sizeof(uint16_t) : 0)
        {
        }

        template<typename T>
        bool get(T & t)
        {
            return get(reinterpret_cast<uint8_t *>(&t), sizeof(T));
        }

        bool get(PacketSegment & seg, size_t len);

    private:
        bool get(uint8_t * out, size_t sizeofOut);

        // TODO "sufficient" is the correct spelling...
        bool sufficentDataAvailable(size_t len) const
        {
            bool ret = pos_ + len <= buff_.size();
            if (!ret)
                LOG_WARN("Attempted to read beyond length of packet");
            return ret;
        }

        Buffer &buff_;
        size_t pos_;
};

template<>
bool Packet::get(std::string_view & seg);

class MSPacketProcessor
{
    public:
        virtual uint16_t getHeader() const = 0;
        virtual bool process(Buffer &buffIn, std::vector<Buffer> &outBuffs, MapleClient &client) = 0;
};

#endif

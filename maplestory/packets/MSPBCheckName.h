/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPBCHECKNAME_H
#define MSPBCHECKNAME_H

#include <cstdint>
#include <string_view>

#include "MSPacketBuilder.h"

class MSPBCheckName : private MSPacketBuilder
{
    public:
        MSPBCheckName(bool available, const std::string_view & name);
        virtual ~MSPBCheckName();

        // TODO Will this pull in both versions of the func?
        using MSPacketBuilder::getRaw;
};

#endif

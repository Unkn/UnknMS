/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPPSELECTWORLD_H
#define MSPPSELECTWORLD_H

#include "MSPacketProcessor.h"

class MSPPSelectWorld : public MSPacketProcessor
{
    public:
        MSPPSelectWorld();
        virtual ~MSPPSelectWorld();

        virtual uint16_t getHeader() const;

        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MSPPSelectWorld *) -> unique_ptr<MSPPSelectWorld, default_delete<MSPPSelectWorld> >;
}

#endif

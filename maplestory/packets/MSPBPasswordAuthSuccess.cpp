/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPBPasswordAuthSuccess.h"

MSPBPasswordAuthSuccess::MSPBPasswordAuthSuccess(
        const MSAccount & account)
{
    uint8_t huh[4] = { 0x2A, 0x79, 0x49, 0x00 };
    uint8_t huh1[2] = { 0x04, 0x65 };
    uint8_t huh2[10] = { 0x00, 0x00, 0x00, 0xA6, 0xB8, 0x9C, 0x2B, 0x4C, 0xC7, 0x01 };

    add<uint16_t>(0); // Header... TODO Is 0 specifically for the this?
    add<uint32_t>(0); // TODO Unknown
    add<uint16_t>(0); // TODO Unknown
    add(huh, 4);

    // Gender
    //
    // 0x0A - Select Gender
    // Will respond with one of the following packets for submit but will wait
    // for a reponse...
    // TODO Find out the reponse packet to send back to the client and allow an
    // option for enabling this configuration opion
    //        Male      17 00 01 00
    //        Female    17 00 01 01
    //
    // Based on the above, 0x00 male, 0x01 female?
    add<uint8_t>(0x00); // TODO Add MSGender to account
    add(huh1);
    add(account.name_);
    add<uint32_t>(0); // TODO Unknown
    add<uint32_t>(0); // TODO Unknown
    add(huh2, 10);
    add<uint32_t>(0); // TODO Unknown

    // TODO See JunoMS send_auth_success_request_pin
    // Frac says this is FILETIME
    //add(created);
}

MSPBPasswordAuthSuccess::~MSPBPasswordAuthSuccess()
{
}

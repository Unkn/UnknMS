/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cstring>
#include <cassert>

// crypto
#include "rand.h"
#include "aes_ofb.h"

#include "MSCrypt.h"
#include "MSVersion.h"
#include "Math.h"
#include "Logger.h"
#include "Debug.h"

namespace MSCrypt
{
    const int EXPANDED_IV_LENGTH = 16;
    // Hard coded aes key used for an AES OFB around the MS proprietary encrypt
    //
    // Found in OdinMS, TitanMS, and Vana src
    const uint8_t MS_AES_256_KEY[] =
    {
        0x13, 0x00, 0x00, 0x00,
        0x08, 0x00, 0x00, 0x00,
        0x06, 0x00, 0x00, 0x00,
        0xB4, 0x00, 0x00, 0x00,
        0x1B, 0x00, 0x00, 0x00,
        0x0F, 0x00, 0x00, 0x00,
        0x33, 0x00, 0x00, 0x00,
        0x52, 0x00, 0x00, 0x00
    };

    // Data used for next IV generation
    //
    // Found in OdinMS, TitanMS, and Vana src
    const uint8_t MS_IV_MOD_TABLE[] =
    {
        0xEC, 0x3F, 0x77, 0xA4, 0x45, 0xD0, 0x71, 0xBF, 0xB7, 0x98, 0x20, 0xFC, 0x4B, 0xE9, 0xB3, 0xE1,
        0x5C, 0x22, 0xF7, 0x0C,	0x44, 0x1B, 0x81, 0xBD, 0x63, 0x8D, 0xD4, 0xC3, 0xF2, 0x10, 0x19, 0xE0,
        0xFB, 0xA1, 0x6E, 0x66, 0xEA, 0xAE, 0xD6, 0xCE, 0x06, 0x18, 0x4E, 0xEB, 0x78, 0x95, 0xDB, 0xBA,
        0xB6, 0x42, 0x7A, 0x2A, 0x83, 0x0B, 0x54, 0x67, 0x6D, 0xE8, 0x65, 0xE7, 0x2F, 0x07, 0xF3, 0xAA,
        0x27, 0x7B, 0x85, 0xB0,	0x26, 0xFD, 0x8B, 0xA9, 0xFA, 0xBE, 0xA8, 0xD7, 0xCB, 0xCC, 0x92, 0xDA,
        0xF9, 0x93, 0x60, 0x2D,	0xDD, 0xD2, 0xA2, 0x9B, 0x39, 0x5F, 0x82, 0x21, 0x4C, 0x69, 0xF8, 0x31,
        0x87, 0xEE, 0x8E, 0xAD, 0x8C, 0x6A, 0xBC, 0xB5, 0x6B, 0x59, 0x13, 0xF1, 0x04, 0x00, 0xF6, 0x5A,
        0x35, 0x79, 0x48, 0x8F,	0x15, 0xCD, 0x97, 0x57, 0x12, 0x3E, 0x37, 0xFF, 0x9D, 0x4F, 0x51, 0xF5,
        0xA3, 0x70, 0xBB, 0x14,	0x75, 0xC2, 0xB8, 0x72, 0xC0, 0xED, 0x7D, 0x68, 0xC9, 0x2E, 0x0D, 0x62,
        0x46, 0x17, 0x11, 0x4D,	0x6C, 0xC4, 0x7E, 0x53, 0xC1, 0x25, 0xC7, 0x9A, 0x1C, 0x88, 0x58, 0x2C,
        0x89, 0xDC, 0x02, 0x64,	0x40, 0x01, 0x5D, 0x38, 0xA5, 0xE2, 0xAF, 0x55, 0xD5, 0xEF, 0x1A, 0x7C,
        0xA7, 0x5B, 0xA6, 0x6F,	0x86, 0x9F, 0x73, 0xE6, 0x0A, 0xDE, 0x2B, 0x99, 0x4A, 0x47, 0x9C, 0xDF,
        0x09, 0x76, 0x9E, 0x30,	0x0E, 0xE4, 0xB2, 0x94, 0xA0, 0x3B, 0x34, 0x1D, 0x28, 0x0F, 0x36, 0xE3,
        0x23, 0xB4, 0x03, 0xD8, 0x90, 0xC8, 0x3C, 0xFE, 0x5E, 0x32, 0x24, 0x50, 0x1F, 0x3A, 0x43, 0x8A,
        0x96, 0x41, 0x74, 0xAC,	0x52, 0x33, 0xF0, 0xD9, 0x29, 0x80, 0xB1, 0x16, 0xD3, 0xAB, 0x91, 0xB9,
        0x84, 0x7F, 0x61, 0x1E,	0xCF, 0xC5, 0xD1, 0x56, 0x3D, 0xCA, 0xF4, 0x05,	0xC6, 0xE5, 0x08, 0x49
    };

    void MSPtyEncrypt(uint8_t * data, size_t len);
    void MSPtyDecrypt(uint8_t * data, size_t len);

    void nextIV(uint8_t iv[IV_LENGTH]);
    void expandIV(uint8_t iv[IV_LENGTH], uint8_t expanded[EXPANDED_IV_LENGTH]);
}

bool MSCrypt::newIV(uint8_t iv[IV_LENGTH])
{
    return generateRandomBytes(iv, IV_LENGTH) > 0;
}

void MSCrypt::nextIV(uint8_t iv[IV_LENGTH])
{
    // This method was ripped from Vana which was a rewrite of the TitanMS
    // version. The Vana version still had uneccessary code so it's not an
    // exact copy anymore :|
    //
    // https://github.com/retep998/Vana.git
    // commit: 19116c07dd0082b6c570bfd6d2ea604947773c10
    // src/common/block_cipher_iv.cpp
    // block_cipher_iv::shuffle

    union
    {
        uint8_t auc[IV_LENGTH];
        uint32_t ui;
    } new_iv = {0xF2, 0x53, 0x50, 0xC6};

	uint8_t input;
	uint8_t value_input;

	for (uint8_t i = 0; i < IV_LENGTH; i++)
    {
		input = iv[i];
		value_input = MS_IV_MOD_TABLE[input];

		new_iv.auc[0] += (MS_IV_MOD_TABLE[new_iv.auc[1]] - input);
		new_iv.auc[1] -= (new_iv.auc[2] ^ value_input);
		new_iv.auc[2] ^= (MS_IV_MOD_TABLE[new_iv.auc[3]] + input);
		new_iv.auc[3] -= (new_iv.auc[0] - value_input);

		new_iv.ui = (new_iv.ui >> 0x1D) | (new_iv.ui << 0x03);
	}

    memcpy(iv, new_iv.auc, IV_LENGTH);
}

void MSCrypt::expandIV(uint8_t iv[IV_LENGTH], uint8_t expanded[EXPANDED_IV_LENGTH])
{
    for (int i = 0; i < EXPANDED_IV_LENGTH; i += IV_LENGTH)
        memcpy(expanded + i, iv, IV_LENGTH);
}

/**
 * @note Copied from JunoMS source by franc[e]sco but looks like it may have
 * originated from TitanMS. The OdinMS one looks fucked like everything else in
 * there...
*
* @param data
* @param len
*/
void MSCrypt::MSPtyEncrypt(uint8_t * data, size_t len)
{
    int32_t j;
    uint8_t a, c;

    for (uint8_t i = 0; i < 3; ++i)
    {
        a = 0;

        for (j = len; j > 0; --j)
        {
            c = data[len - j];
            c = Math::rotl(c, 3);
            c = (uint8_t)((int32_t)c + j);
            c ^= a;
            a = c;
            // TODO Check disassembly to determine if this was pointless (godbolt)
            c = Math::rotr(a, Math::rotbits<uint8_t>(j));
            c ^= 0xFF;
            c += 0x48;
            data[len - j] = c;
        }

        a = 0;

        for (j = len; j > 0; --j)
        {
            c = data[j - 1];
            c = Math::rotl(c, 4);
            c = (uint8_t)((int32_t)c + j);
            c ^= a;
            a = c;
            c ^= 0x13;
            c = Math::rotr(c, 3);
            data[j - 1] = c;
        }
	}
}

/**
 * @note Copied from JunoMS source by franc[e]sco but looks like it may have
 * originated from TitanMS. The OdinMS one looks fucked like everything else in
 * there...
 *
 * @param data
 * @param len
 */
void MSCrypt::MSPtyDecrypt(uint8_t * data, size_t len)
{
    int32_t j;
    uint8_t a, b, c;

    for (uint8_t i = 0; i < 3; ++i)
    {
        a = 0;
        b = 0;

        for (j = len; j > 0; --j)
        {
            c = data[j - 1];
            c = Math::rotl(c, 3);
            c ^= 0x13;
            a = c;
            c ^= b;
            c = (uint8_t)((int32_t)c - j);
            c = Math::rotr(c, 4);
            b = a;
            data[j - 1] = c;
        }

        a = 0;
        b = 0;

        for (j = len; j > 0; --j)
        {
            c = data[len - j];
            c -= 0x48;
            c ^= 0xFF;
            // TODO Check disassembly to determine if this was pointless
            c = Math::rotl(c, Math::rotbits<uint8_t>(j));
            a = c;
            c ^= b;
            c = (uint8_t)((int32_t)c - j);
            c = Math::rotr(c, 3);
            b = a;
            data[len - j] = c;
        }
    }
}

void MSCrypt::generateHeader(
        uint8_t header[HEADER_LENGTH],
        uint16_t len,
        const uint8_t iv[IV_LENGTH])
{
    // Header generation from Franc[e]sco's JunoMS source. Was less than 1/4
    // the instructions of the TitanMS version.
    //
    // Godbolt says i reduced francs by 1/4 ;)
    //
    // TitanMS: 36
    // JunoMS:  8
    // UnknMS:  6

    uint16_t & lowpart = *reinterpret_cast<uint16_t *>(header);
    uint16_t & hipart = *reinterpret_cast<uint16_t *>(header + 2);

    lowpart = *reinterpret_cast<const uint16_t *>(iv + 2);
    // TODO Consider passing the game version as a parameter if we are able to
    // support multiple versions with the same source via config
    uint16_t version = 0xFFFF - MSVersion::GAME_VERSION;
    lowpart ^= version;
    hipart = lowpart ^ len;

    LOG_DEBUG("Generated header {} with input {:d}, {}",
            printableBytes(header, HEADER_LENGTH), len, printableBytes(iv, IV_LENGTH));
}

void MSCrypt::encrypt(uint8_t * data, size_t len, uint8_t iv[IV_LENGTH])
{
    // Should pass in a second buffer to this function now for the output :|
    auto enc = std::unique_ptr<uint8_t []>(new uint8_t[len]);
    uint8_t expandedIV[EXPANDED_IV_LENGTH];
    expandIV(iv, expandedIV);

    MSPtyEncrypt(data, len);
    if (len > 1456)
    {
        // There was some code in TitanMS's source where if the message was larger
        // than 1456 bytes, it would chunk the encryption for 1456 bytes then, 1460
        // bytes thereafter. Not sure why, but we should at least have some code to
        // warn us about data that would have hit that case so we know why there is
        // somethin fuck up if it was necessary...
        LOG_DEBUG("Packet data of length {:d} being AES256 OFB encrypted... "
                "Titan MS would have done the encryption in chunks", len);
    }
    int encLen = aes256_ofb_encrypt(data, len, MS_AES_256_KEY, expandedIV, enc.get());
    assert(encLen == (int)len);
    memcpy(data, enc.get(), len);
    nextIV(iv);
}

void MSCrypt::decrypt(uint8_t * data, size_t len, uint8_t iv[IV_LENGTH])
{
    auto dec = std::unique_ptr<uint8_t []>(new uint8_t[len]);
    uint8_t expandedIV[EXPANDED_IV_LENGTH];
    expandIV(iv, expandedIV);

    int decLen = aes256_ofb_decrypt(data, len, MS_AES_256_KEY, expandedIV, dec.get());
    assert(decLen == (int)len);
    nextIV(iv);
    MSPtyDecrypt(dec.get(), len);
    memcpy(data, dec.get(), len);
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPACKETBUILDER_H
#define MSPACKETBUILDER_H

#include <cassert>
#include <string_view>

#include "buffer.hpp"

/**
 * @note Vana inspired I guess? see vana::packet_builder
 * @todo See the note in MSPacketProcessor for Packet about a structure
 * overlay. This will work out for generic shit but to reduce instructions for
 * packet processing we probably want to just do struct overlays on the raw
 * buffers.
 */
class MSPacketBuilder
{
    public:
        MSPacketBuilder();
        MSPacketBuilder(const MSPacketBuilder & obj);
        MSPacketBuilder(MSPacketBuilder && obj);
        virtual ~MSPacketBuilder();

        MSPacketBuilder & operator=(const MSPacketBuilder & rhs);
        MSPacketBuilder & operator=(MSPacketBuilder && rhs);

        template<typename T>
        MSPacketBuilder & add(const T & value);

#if 0
        template<typename T>
        MSPacketBuilder & reserve(const T & value);
#endif

        template<typename T>
        MSPacketBuilder & add(const T * values, size_t len);

        Buffer & getRaw();
        const Buffer & getRaw() const;

    private:
        Buffer raw_;
};

template<typename T>
MSPacketBuilder & MSPacketBuilder::add(const T & value)
{
    // TODO To reduce the template code make a subroutine of like
    //     MSPacketBuild & addLittleEndian(const uint16_t * data, size_t sizeofData);
    // which does the switch
    switch (sizeof(T))
    {
        case 1:
            raw_.write(value);
            break;

        case 2:
        {
            uint16_t i = htole16(*reinterpret_cast<const uint16_t *>(&value));
            raw_.write(i);
            break;
        }

        case 4:
        {
            uint32_t i = htole32(*reinterpret_cast<const uint32_t *>(&value));
            raw_.write(i);
            break;
        }

        case 8:
        {
            uint64_t i = htole64(*reinterpret_cast<const uint64_t *>(&value));
            raw_.write(i);
            break;
        }

        default:
            assert(!"Unsupported default input for default template for packet builder add");
            break;
    }
    return *this;
}

template<>
MSPacketBuilder & MSPacketBuilder::add(const std::string_view & value);
template<>
MSPacketBuilder & MSPacketBuilder::add(const std::string & value);

#if 0
template<typename T>
MSPacketBuilder & MSPacketBuilder::reserve(const T & value)
{
}
#endif

template<typename T>
MSPacketBuilder & MSPacketBuilder::add(const T * values, size_t len)
{
    raw_.write(values, len);
    return *this;
}

// TODO Better idea?
#include "MSPBData.h"

#endif

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "Zeroize.h"

#include "MSPacketHeaders.h"
#include "MSPPLoginUser.h"
#include "MSLoginClient.h"
#include "MSPBLoginError.h"
#include "MSPBPasswordAuthSuccess.h"

MSPPLoginUser::MSPPLoginUser(
        const std::shared_ptr<HSMConnection> & hsmconn,
        const std::shared_ptr<MSDBAccount> & msdbaccount) :
    hsmconn_(hsmconn),
    msdbaccount_(msdbaccount)
{
}

MSPPLoginUser::~MSPPLoginUser()
{
    // Pineapples
}

uint16_t MSPPLoginUser::getHeader() const
{
    return MSPacketHeaders::v55::LOGIN_USER;
}

bool MSPPLoginUser::process(
        Buffer &buffIn,
        std::vector<Buffer> & outBuffs,
        MapleClient &client)
{
    bool ok = true;
    auto & loginClient = static_cast<MSLoginClient &>(client);

    std::string_view user;
    std::string_view pass;

    // TODO If the 'Packet' idea works out well, turn that into an input
    Packet p(buffIn, true);

    p.get(user);
    p.get(pass);

    // TODO In my VM these are the remaining bytes. Is this the HWID or somethin?
    // 08 00 27 E0 D0 67 31 B7 2B 88 00 00 00 00 17 8C 00 00 00 00 02 00 00

    LOG_DEBUG("Login request: [{}:{}]", user, pass);

    auto account = msdbaccount_->get(user);
    if (!account)
    {
        auto builder = MSPBLoginError(MSLoginError::InvalidUser);
        outBuffs.emplace_back(std::move(builder.getRaw()));
    }
    else if (!password_auth(pass, *account->auth_))
    {
        auto builder = MSPBLoginError(MSLoginError::WrongPassword);
        outBuffs.emplace_back(std::move(builder.getRaw()));
    }
    else
    {
        // TODO Update account last_access on successful auth

        // TODO cli or compile option?
        // TODO Check if pin exists to determine next stage
        //loginClient.loginStatus_ = MSLoginStatus::NewPin;
        loginClient.loginStatus_ = MSLoginStatus::RequestPin;

        account->auth_.reset();
        auto kaccount = std::unique_ptr<const MSAccount>(account.release());
        client.account_ = std::move(kaccount);

        auto builder = MSPBPasswordAuthSuccess(*client.account_);
        outBuffs.emplace_back(std::move(builder.getRaw()));
    }

    // Zeroize the password
    zeroize(const_cast<char *>(pass.data()), pass.size());

    return ok;
}

bool MSPPLoginUser::password_auth(
        const std::string_view & pass,
        const MSAuthData & auth_data) const
{
    bool ret = false;
    size_t len = pass.size() + MS_ACCOUNT_SALT_LEN;
    auto input = std::make_unique<uint8_t []>(len);
    auto raw_input = input.get();
    uint8_t hmac[SHA256_DIGEST_LEN];

    memcpy(raw_input, pass.data(), pass.size());
    memcpy(raw_input + pass.size(), auth_data.salt_, MS_ACCOUNT_SALT_LEN);

    if (!hsmconn_->hmac_sha256(input.get(), len, hmac))
        LOG_ERROR("Failed to get HMAC for password auth from HSM");
    else if (memcmp(hmac, auth_data.hmac_, SHA256_DIGEST_LEN) != 0)
        LOG_ERROR("HMAC mismatch for password auth");
    else
        ret = true;

    // Why not zeroize the password :|
    zeroize(raw_input, pass.size());

    return ret;
}

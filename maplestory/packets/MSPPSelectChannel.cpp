/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPPSelectChannel.h"

#include <cassert>

#include "Logger.h"
#include "Types.h"
#include "MSPBChannelCharacters.h"
#include "MSPacketHeaders.h"
#include "MSLoginClient.h"

MSPPSelectChannel::MSPPSelectChannel(
        const std::shared_ptr<MSDBCharacter> & msdb_char) :
    msdb_char_(msdb_char)
{
}

MSPPSelectChannel::~MSPPSelectChannel()
{
}

uint16_t MSPPSelectChannel::getHeader() const
{
    return MSPacketHeaders::v55::SELECT_CHANNEL;
}

bool MSPPSelectChannel::process(
        Buffer &buffIn,
        std::vector<Buffer> & outBuffs,
        MapleClient &client)
{
    // 19 00     00     00
    // header, channel id, ???
    // TODO Whats the final byte
    bool ret = false;
    uint8_t channel; // TODO May want to associate this with the client
    auto & lc = static_cast<MSLoginClient &>(client);

    Packet p(buffIn, true);

    if (lc.loginStatus_ != MSLoginStatus::ChannelSelect)
    {
        LOG_WARN("Client {:d} attempted to select channel with invalid login status {:d}",
                client.getConnectionID(), underlying(lc.loginStatus_));
    }
    // TODO Validate the channel
    else if (!p.get(channel))
    {
        LOG_WARN("Failed to parse channel select packet from Client {:d}",
                client.getConnectionID());
    }
    else
    {
        assert(client.account_ != nullptr);

        LOG_DEBUG("Client {:d} requested channel {:d}", client.getConnectionID(), channel);

        // TODO Check if account has been set yet
        auto account_id = client.account_->id_;
        auto & chars = client.characters_;

        // TODO Only grab the characters for the given world
        // TODO Might be able to hack together a way to allow more than 3
        // characters to be created per world by using the channel number to
        // identify groupings of 3 characters
        if (!msdb_char_->get_characters(account_id, chars))
        {
            LOG_ERROR("Failed to retrieve characters for account {:d}", account_id);
        }
        else
        {
            ret = true;
            lc.loginStatus_ = MSLoginStatus::CharacterSelect;

            auto builder = MSPBChannelCharacters(chars);
            outBuffs.emplace_back(std::move(builder.getRaw()));
        }
    }

    return ret;
}

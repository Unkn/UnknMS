/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSLOGINCLIENT_H
#define MSLOGINCLIENT_H

#include "MapleClient.h"

enum class MSLoginStatus
{
    None,
    NewPin,
    RequestPin,
    CheckPin,
    LoggedIn,
    WorldSelect,
    ChannelSelect,
    CharacterSelect,
};

class MSLoginClient : public MapleClient
{
    public:
        MSLoginClient(std::unique_ptr<Socket> && socket);
        virtual ~MSLoginClient();

        // TODO This name may be redundant and can be reduced to status_
        MSLoginStatus loginStatus_;
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MSLoginClient *) -> unique_ptr<MSLoginClient, default_delete<MSLoginClient> >;
}

#endif

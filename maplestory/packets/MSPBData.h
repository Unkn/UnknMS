/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPBDATA_H
#define MSPBDATA_H

#include "MSPacketBuilder.h"
#include "MSCharacter.h"

class MSCharacterDisplay
{
    public:
        MSCharacterDisplay(const MSCharacter & c) :
            c_(c)
        {
        }

        const MSCharacter & operator->() const
        {
            return c_;
        }

        const MSCharacter & operator*() const
        {
            return c_;
        }

    private:
        const MSCharacter & c_;
};

template<> MSPacketBuilder & MSPacketBuilder::add(const MSCharacter & c);
template<> MSPacketBuilder & MSPacketBuilder::add(const MSCharacterDisplay & cd);

#endif

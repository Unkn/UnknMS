/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPBCHANNELCHARACTERS_H
#define MSPBCHANNELCHARACTERS_H

#include <vector>
#include <memory>

#include "MSPacketBuilder.h"
#include "MSCharacter.h"

class MSPBChannelCharacters : private MSPacketBuilder
{
    public:
        MSPBChannelCharacters(const std::vector<std::unique_ptr<MSCharacter> > & characters);
        virtual ~MSPBChannelCharacters();

        using MSPacketBuilder::getRaw;
};

#endif

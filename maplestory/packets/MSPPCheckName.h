/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPPCHECKNAME_H
#define MSPPCHECKNAME_H

#include "MSPacketProcessor.h"
#include "MSDBCharacter.h"

class MSPPCheckName : public MSPacketProcessor
{
    public:
        MSPPCheckName(const std::shared_ptr<MSDBCharacter> & msdb_char);
        virtual ~MSPPCheckName();

        virtual uint16_t getHeader() const;
        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);

    private:
        std::shared_ptr<MSDBCharacter> msdb_char_;
};

namespace std
{
    unique_ptr(MSPPCheckName *) -> unique_ptr<MSPPCheckName, default_delete<MSPPCheckName> >;
}

#endif

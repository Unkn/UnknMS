/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPPCreateCharacterTests.h"

#include "MSPacketHeaders.h"

using testing::_;
using testing::Return;

TEST_F(MSPPCreateCharacterTests, RequireCharacterSelectStatus)
{
    // Just pick some status that isnt CharacterSelect. Can make a parameterized test later
    client_.loginStatus_ = MSLoginStatus::WorldSelect;

    ASSERT_FALSE(mspp_->process(in_, out_, client_));
}

TEST_F(MSPPCreateCharacterTests, ExceedMaxCharsOnAccount)
{
    // Character limit is 3 max per world so creation at 3 or greater is
    // expected to fail
    client_.characters_.resize(3);

    ASSERT_FALSE(mspp_->process(in_, out_, client_));
}

TEST_F(MSPPCreateCharacterTests, NoReservedName)
{
    client_.reserved_name_.clear();

    ASSERT_FALSE(mspp_->process(in_, out_, client_));
}

TEST_F(MSPPCreateCharacterTests, NotMatchingReservedName)
{
    client_.reserved_name_ = "banana";

    ASSERT_FALSE(mspp_->process(in_, out_, client_));
}

TEST_F(MSPPCreateCharacterTests, FailedToAddToDB)
{
    EXPECT_CALL(*msdb_char_, create_character(_, _)).
        WillOnce(Return(false));

    ASSERT_TRUE(mspp_->process(in_, out_, client_));
    ASSERT_EQ(1, (int)out_.size());

    uint16_t h;
    Packet p(out_[0]);
    p.get(h);

    // the login response packet is used to display an error
    ASSERT_EQ(h, MSPacketHeaders::v55::LOGIN_RESPONSE);
}

TEST_F(MSPPCreateCharacterTests, SuccessfullyCreatedCharacter)
{
    EXPECT_CALL(*msdb_char_, create_character(_, _)).
        WillOnce(Return(true));

    ASSERT_TRUE(mspp_->process(in_, out_, client_));
    ASSERT_EQ(1, (int)out_.size());

    uint16_t h;
    Packet p(out_[0]);
    p.get(h);

    // On success a show character packet will be sent to the client
    ASSERT_EQ(h, MSPacketHeaders::v55::SHOW_CHARACTER);
}

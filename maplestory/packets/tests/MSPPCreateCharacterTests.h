/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cassert>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "MSPPCreateCharacter.h"
#include "MSDBCharacterMock.h"
#include "MSLoginClientMock.h"

class MSPPCreateCharacterTests : public ::testing::Test
{
    protected:
        virtual void SetUp()
        {
            uint8_t sample_packet[] =
            {
                0x0E, 0x00,
                0x04, 0x00,
                0x00, 0x00, 0x00, 0x00,
                0x20, 0x4E, 0x00, 0x00,
                0x4E, 0x75, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00,
                0x82, 0xDE, 0x0F, 0x00,
                0xA2, 0x2C, 0x10, 0x00,
                0x81, 0x5B, 0x10, 0x00,
                0xF0, 0xDD, 0x13, 0x00,
                0x00,
                0x06,
                0x05,
                0x06,
                0x08,
            };

            client_.reserved_name_ = "alex";
            // Sample packet has name length set to 4
            assert(client_.reserved_name_.size() == 4);
            memcpy(sample_packet + 4, client_.reserved_name_.data(), 4);

            // prepare with arguments that should succeed
            client_.loginStatus_ = MSLoginStatus::CharacterSelect;
            client_.account_ = std::make_unique<MSAccount>();

            in_.write(sample_packet, sizeof(sample_packet));

            msdb_char_ = std::make_shared<MSDBCharacterMock>();
            mspp_ = std::make_unique<MSPPCreateCharacter>(msdb_char_);
        }

        Buffer in_;
        std::vector<Buffer> out_;

        std::shared_ptr<MSDBCharacterMock> msdb_char_;
        MSLoginClientMock client_;
        std::unique_ptr<MSPPCreateCharacter> mspp_;
};

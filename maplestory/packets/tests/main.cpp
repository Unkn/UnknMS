/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "Logger.h"
#include "StdOutLogMethod.h"

int main(int argc, char ** argv)
{
    // Why not :| Can have a compile time option to omit this...
    USE_STDOUT_LOG_METHOD;

    // Since Google Mock depends on Google Test, InitGoogleMock() is
    // also responsible for initializing Google Test.  Therefore there's
    // no need for calling testing::InitGoogleTest() separately.
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}

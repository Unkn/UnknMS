/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPBLoginError.h"

#include <type_traits>

#include "MSPacketHeaders.h"

MSPBLoginError::MSPBLoginError(MSLoginError error)
{
    add(MSPacketHeaders::v55::LOGIN_RESPONSE);
    // TODO underlying_type
    add(static_cast<uint16_t>(error));
    add<uint32_t>(0); // TODO ?
}

MSPBLoginError::~MSPBLoginError()
{
    // Pineapples
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPBLOGINERROR_H
#define MSPBLOGINERROR_H

#include <cstdint>

#include "MSPacketBuilder.h"

enum class MSLoginError : uint16_t
{
    // TODO Are there other errors available?
    // TODO Check these are correct, came from TitanMS source
    // 0x00 end of file error (could this packet need more data?)
    // 0x01 Nothing happens (could this packet need more data?)
    // 0x02 end of file error (could this packet need more data?)
    DeletedOrBlocked    = 0x03,
    WrongPassword       = 0x04,
    InvalidUser         = 0x05, // Not a registered id
    SystemError         = 0x06,
    AlreadyLoggedIn     = 0x07, // Or server under inspection
    // 0x08 system error
    // 0x09 system error
    // 0x0A Too many server connection requests
    // 0x0B Age 20+ channel
    // 0x0C end of file error (could this packet need more data?)
    // 0x0D Unable to login as GM as current IP
    // 0x0E Wrong gateway or haven't changed personal information yet (has nexon KR button)
    // 0x0F Pending account processing, no access yet (has nexon KR button)
    // 0x10 Verify account via email to play
    // 0x11 Wrong gateway or haven't changed personal information yet (no button)
    // 0x12 Nothing happens (could this packet need more data?)
    // 0x13 Nothing happens (could this packet need more data?)
    // 0x14 Nothing happens (could this packet need more data?)
    // 0x15 Verify account via email to play
    // 0x16 Nothing happens (could this packet need more data?)
    // 0x17 EULA
    // 0x18 Nothing happens (could this packet need more data?)
    // 0x19 Greetings go to maple europe to play
    // 0x1A Nothing happens (could this packet need more data?)
    // 0x1B Download full client blueblox
    //
    // TODO: Doesn't appear to be anything else after that. Also given 0x00 has
    // an end of file error there is either additional data required for all
    // 0xXX00 responses or it is possible that these are 1 byte instead of 2.
    // Debug the client to find out.
};

class MSPBLoginError : private MSPacketBuilder
{
    public:
        MSPBLoginError(MSLoginError error);
        virtual ~MSPBLoginError();

        // TODO Will this pull in both versions of the func?
        using MSPacketBuilder::getRaw;
};

#endif

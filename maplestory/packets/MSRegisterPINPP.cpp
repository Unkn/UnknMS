/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <algorithm>

#include "Logger.h"
#include "MSRegisterPINPP.h"
#include "MSPacketHeaders.h"
#include "MSLoginClient.h"
#include "MSPackets.h"

MSRegisterPINPP::MSRegisterPINPP()
{
}

MSRegisterPINPP::~MSRegisterPINPP()
{
}

uint16_t MSRegisterPINPP::getHeader() const
{
    return MSPacketHeaders::v55::REGISTER_PIN;
}

bool MSRegisterPINPP::process(
        Buffer &buffIn,
        std::vector<Buffer> & outBuffs,
        MapleClient &client)
{
    // Submit with pin 1234
    //     05 00 01 04 00 30 31 32 33
    //
    // Cancel
    //     05 00 00

    auto & loginClient = static_cast<MSLoginClient &>(client);
    bool ret = true;

    // TODO Should be able to send in a series of parameters and parse into
    // them with a single 'get' call like 'getMult` or something. Maybe with
    // tuple?
    uint16_t header;
    uint8_t submit;

    Packet p(buffIn);

    p.get(header);
    p.get(submit);

    if (submit == 0)
    {
        loginClient.loginStatus_ = MSLoginStatus::None;
        LOG_NOTICE("Client {} cancelled pin submission", loginClient.getConnectionID());
    }
    else if (submit == 1)
    {
        // We could probably use a lambda to pass in a validator so that
        // Packet::get(PacketSegmetnt) can be used to get the length,
        // validate it, then get the data view all in one call in a generic
        // manner.
        uint16_t pinLen;
        PacketSegment pin;

        p.get(pinLen);

        if (pinLen == 4)
        {
            p.get(pin, pinLen);

            if (!std::all_of(pin.data_, pin.data_ + pin.len_, isdigit))
            {
                LOG_WARN("Recieved invalid pin char from client {}", loginClient.getConnectionID());
                ret = false;
            }
        }

        if (ret)
        {
            LOG_DEBUG("Got pin {}", std::string((const char *)pin.data_, pin.len_));

            // TODO Maybe do a check for a reused PIN to reject it or somethin
            // TODO Store PIN in DB
            // Zeroize pin when done
            memset(pin.data_, 0, pin.len_);

            // We could send a PIN request instead but since the pin was just
            // registered we will count that towards the PIN login.
            //
            // TODO This could be a configurable option on whether or not to
            // ask after register.
            // TODO We should tie these 2 functions together so that the login
            // status is appropriately set automatically
            auto builder = MSPackets::loginProcess(MSPackets::LoginRsp::LoginSuccess);
            outBuffs.emplace_back(std::move(builder.getRaw()));
            loginClient.loginStatus_ = MSLoginStatus::LoggedIn;
        }
    }
    else
    {
        // TODO We may want to log hte full packets on error to see if they are
        // being maliciously formed for other things to track shit maybe
        LOG_WARN("Recieved unsupported 'submit' {:02X} for packet {:04X}", submit, header);
        ret = false;
    }

    return ret;
}

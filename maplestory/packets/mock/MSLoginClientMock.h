/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "MSLoginClient.h"

class MSLoginClientMock : public MSLoginClient
{
    public:
        MSLoginClientMock() :
            MSLoginClient(nullptr)
        {
        }

        virtual ~MSLoginClientMock() {}

        MOCK_METHOD1(name_available, bool(std::string_view));
        MOCK_METHOD1(release_names, bool(uint32_t));
        MOCK_METHOD0(clear_all_reserved_names, bool());
        MOCK_METHOD2(create_character, bool(uint32_t, MSCharacter &));
        MOCK_METHOD2(get_characters, bool(uint32_t, std::vector<std::unique_ptr<MSCharacter> > &));
};

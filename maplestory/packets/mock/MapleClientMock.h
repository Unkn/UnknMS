/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "MapleClient.h"

class MapleClientMock : public MapleClient
{
    public:
        MapleClientMock() :
            MapleClient(nullptr)
        {
        }

        virtual ~MapleClientMock() {}

        MOCK_METHOD0(establishConnection, bool());
        MOCK_METHOD2(readPacket, bool(Buffer &, bool));
        MOCK_METHOD2(sendPacket, bool(Buffer &, bool));
};

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <type_traits>

#include "portable_endian.h"

#include "MSPackets.h"
#include "MSPacketHeaders.h"

MSPacketBuilder MSPackets::connection(
        uint16_t gameVersion,
        const std::string & gameSubversion,
        uint8_t gameLocale,
        uint8_t recvIV[MSCrypt::IV_LENGTH],
        uint8_t sendIV[MSCrypt::IV_LENGTH])
{
    // OdinMS v40 method
    //
    // this is like almost 100% fucking wrong :|
    //
    //  2 byte header - 0x0E (14), see sendops.properties
    //  2 byte game version
    //  2 byte subversion ? - value 1
    //  4 (or 1?) byte unknown - value 0
    //  4 (or 1?) byte recv iv
    //  4 (or 1?) byte send iv
    //  4 (or 1?) byte game locale - value 5
    //
    // Vana v75 method
    //
    // closer to correct but was poorly documented
    //
    //  2 byte header - size of packet body
    //  2 byte game version
    //  variable string length subversion - login value "0"
    //  4 byte recv iv
    //  4 byte send iv
    //  1 byte game locale
    //
    //  Implementing the OdinMS one since using v40 client... tweak it till it
    //  works unless we get to the point where we need to debug the client

    uint16_t subverlen = static_cast<uint16_t>(gameSubversion.size());
    uint16_t handshakeLen = 2 + 2 + subverlen + 4 + 4 + 1;
    MSPacketBuilder ret;

    // Header is 2 byte length of handshake packet excluding the header itself
    // and the subversion length
    ret.add(handshakeLen);
    ret.add(gameVersion);
    ret.add(subverlen);
    // Apparently the subversion is written even when there is no subversion
    // data. Does this mean it's null terminated and the subversionlen right
    // before doesnt really matter? :|
    ret.add(gameSubversion.data(), subverlen);
    ret.add(recvIV, MSCrypt::IV_LENGTH);
    ret.add(sendIV, MSCrypt::IV_LENGTH);
    ret.add(gameLocale);

    return ret;
}

MSPacketBuilder MSPackets::loginProcess(LoginRsp rsp)
{
    MSPacketBuilder ret;

    // TODO Move to list of headers when we have a name for it...
    const uint16_t header = 0x0D;

    ret.add(header);
    ret.add(static_cast<typename std::underlying_type<LoginRsp>::type>(rsp));

    return ret;
}

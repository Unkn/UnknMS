/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPPWORLDSELECT_H
#define MSPPWORLDSELECT_H

#include "MSPacketProcessor.h"

class MSPPWorldSelect : public MSPacketProcessor
{
    public:
        MSPPWorldSelect();
        virtual ~MSPPWorldSelect();

        virtual uint16_t getHeader() const;

        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);
};

#endif

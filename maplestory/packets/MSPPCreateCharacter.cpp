/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPPCreateCharacter.h"

#include "Types.h"
#include "MSPacketHeaders.h"
#include "MSCharacterFactory.h"
// TODO We may want to rename this from LoginError to just Error
#include "MSPBLoginError.h"
#include "MSPBShowCharacter.h"
#include "MSLoginClient.h"

// Limit imposed by the character select. Only 3 characters can be viewed.
#define MAX_CHARACTERS 3

MSPPCreateCharacter::MSPPCreateCharacter(
        const std::shared_ptr<MSDBCharacter> & msdb_char) :
    msdb_char_(msdb_char)
{
}

MSPPCreateCharacter::~MSPPCreateCharacter()
{
    // Pineapple
}

uint16_t MSPPCreateCharacter::getHeader() const
{
    return MSPacketHeaders::v55::CREATE_CHARACTER;
}

bool MSPPCreateCharacter::process(
        Buffer &buffIn,
        std::vector<Buffer> & outBuffs,
        MapleClient &client)
{
    // Example Packet:
    //
    // 0E 00            header
    // 05 00            name_len (15 max according to titanms?)
    // 61 61 61 61 61   name
    // 20 4E 00 00      face
    // 4E 75 00 00      hair
    // 00 00 00 00      hair color (could be just 1 byte with 3 unknown bytes)
    // 00 00 00 00      skin
    // 82 DE 0F 00      equip 0x05
    // A2 2C 10 00      equip 0x06
    // 81 5B 10 00      equip 0x07
    // F0 DD 13 00      equip 0x0B
    // 00               gender 0 male 1 female
    // 06               str
    // 05               dex
    // 06               int
    // 08               luk

    bool ok = false;
    Packet p(buffIn, true);
    MSCharacterFactory::NewCharArgs a;
    std::unique_ptr<MSCharacter> new_char;
    auto & lc = static_cast<MSLoginClient &>(client);
    const MSAccount * msa = client.account_.get();

    if (lc.loginStatus_ != MSLoginStatus::CharacterSelect)
    {
        LOG_WARN("Client {:d} attempted to create a character with invalid login status {:d}",
                client.getConnectionID(), underlying(lc.loginStatus_));
    }
    else if (client.characters_.size() >= MAX_CHARACTERS)
    {
        LOG_WARN("Client {:d} attempted to create more characters than the allowed max",
                msa->id_);
    }
    else if (client.reserved_name_.empty())
    {
        LOG_WARN("Client {:d} requested to create character before reserving a name",
                msa->id_);
    }
    else if (!(p.get(a.name_) &&
               p.get(a.face_) &&
               p.get(a.hair_) &&
               p.get(a.hair_color_) &&
               p.get(a.skin_) &&
               p.get(a.coat_) &&
               p.get(a.pants_) &&
               p.get(a.shoes_) &&
               p.get(a.weapon_) &&
               p.get(a.gender_) &&
               p.get(a.str_) &&
               p.get(a.dex_) &&
               p.get(a.int_) &&
               p.get(a.luk_)))
    {
        LOG_WARN("Failed to decode character creation packet");
    }
    else if (a.name_ != client.reserved_name_)
    {
        LOG_WARN("Name '{}' requested by {:d} for new character does not match reserved name '{}'",
                a.name_, msa->id_, client.reserved_name_);
    }
    else if (!(new_char = MSCharacterFactory::new_character(a)))
    {
        // TODO Log packet and info about the connection
        LOG_WARN("Invalid new character configuration");
    }
    else
    {
        ok = true;

        if (!msdb_char_->create_character(msa->id_, *new_char))
        {
            LOG_ERROR("Failed to add new character to db for account {:d}", msa->id_);

            // TODO Not exactly the right error for the situation but w/e. Work it out later.
            auto builder = MSPBLoginError(MSLoginError::SystemError);
            outBuffs.emplace_back(std::move(builder.getRaw()));
        }
        else
        {
            LOG_INFO("Successfully created character {} ({:d}) for account {:d}",
                    new_char->name_, new_char->dbid_, msa->id_);

            client.reserved_name_.clear();

            auto builder = MSPBShowCharacter(*new_char);
            outBuffs.emplace_back(std::move(builder.getRaw()));
        }
    }

    return ok;
}


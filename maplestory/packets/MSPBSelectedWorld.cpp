/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPBSelectedWorld.h"

#include "MSPacketHeaders.h"

MSPBSelectedWorld::MSPBSelectedWorld()
{
    add(MSPacketHeaders::v55::SELECTED_WORLD);
    add<uint16_t>(0); // TODO whats this? play with values or debug client
}

MSPBSelectedWorld::~MSPBSelectedWorld()
{
}

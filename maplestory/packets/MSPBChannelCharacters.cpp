/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPBChannelCharacters.h"

#include "MSPacketHeaders.h"
#include "MSPBData.h"

MSPBChannelCharacters::MSPBChannelCharacters(
        const std::vector<std::unique_ptr<MSCharacter> > & characters)
{
    add(MSPacketHeaders::v55::CHANNEL_CHARS);
    add<uint8_t>(0); // TODO Unknown
    add<uint8_t>(characters.size());
    for (auto & c : characters)
        add<MSCharacterDisplay>(*c);
}

MSPBChannelCharacters::~MSPBChannelCharacters()
{
}

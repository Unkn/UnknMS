/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSCRYPT_H
#define MSCRYPT_H

#include <cstddef>
#include <cstdint>

namespace MSCrypt
{
    const int IV_LENGTH = 4;
    const int HEADER_LENGTH = 4;

    bool newIV(uint8_t iv[IV_LENGTH]);

    void generateHeader(uint8_t header[HEADER_LENGTH], uint16_t len, const uint8_t iv[IV_LENGTH]);

    void encrypt(uint8_t * data, size_t len, uint8_t iv[IV_LENGTH]);
    void decrypt(uint8_t * data, size_t len, uint8_t iv[IV_LENGTH]);
}

#endif

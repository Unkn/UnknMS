/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSWORLD_H
#define MSWORLD_H

#include <cstdint>
#include <string>

class MSWorld
{
    public:
        MSWorld(uint8_t id, uint8_t channels, uint8_t type, const std::string & name);
        virtual ~MSWorld();

        uint8_t id_;
        uint8_t channels_;
        // TODO TitanMS has a remark "Type 2-new". May want to enum the types.
        uint8_t type_;
        std::string name_;
};

#endif

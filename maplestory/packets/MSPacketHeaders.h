/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPACKETHEADERS_H
#define MSPACKETHEADERS_H

#include <cstdint>

namespace MSPacketHeaders
{
    // TODO Config file for each version
    // TODO This really should just be an enum class with underlying type specified :|
    // Should we sort these?
    namespace v55
    {
        // TODO recv namespace
        const uint16_t LOGIN_USER       = 0x001B;
        const uint16_t HANDLE_LOGIN     = 0x0003;
        const uint16_t REGISTER_PIN     = 0x0005;
        const uint16_t SHOW_WORLDS      = 0x0018;
        const uint16_t SELECT_WORLD     = 0x0013;
        const uint16_t SELECT_CHANNEL   = 0x0019;
        const uint16_t CONNECT_CHANNEL  = 0x0014;
        const uint16_t CONNECT_GAME     = 0x0016;
        const uint16_t CHECK_NAME       = 0x0009;
        const uint16_t CREATE_CHARACTER = 0x000E;
        // TODO 0x0015 - Accepted EULA, see MSLoginError 0x17

        // TODO send namespace
        // TODO Apparenty these responses can be used where the name check is
        // performed in the client :| ... investigate
        const uint16_t LOGIN_RESPONSE   = 0x0000;
        const uint16_t SHOW_WORLD_LIST  = 0x0005;
        const uint16_t CHECK_NAME_RSP   = 0x0006;
        const uint16_t SHOW_CHARACTER   = 0x0007;
        const uint16_t SELECTED_WORLD   = 0x0012;
        const uint16_t CHANNEL_CHARS    = 0x0013;
    }

    namespace v40
    {
        // GENERAL
        const uint16_t PING = 0x0A;
        const uint16_t CLIENT_HELLO = 0x0E;

        // LOGIN
        const uint16_t LOGIN_STATUS = 0x01;
        const uint16_t SERVERSTATUS = 0x02;
        const uint16_t SERVERLIST = 0x03;
        const uint16_t CHARLIST = 0x04;
        const uint16_t SERVER_IP = 0x05;
        const uint16_t CHAR_NAME_RESPONSE = 0x06;
        const uint16_t ADD_NEW_CHAR_ENTRY = 0x07;
        const uint16_t DELETE_CHAR_RESPONSE = 0x08;

        // CHANNEL
        const uint16_t SPAWN_PLAYER = 0x3C;
        const uint16_t REMOVE_PLAYER_FROM_MAP = 0x3D;
        const uint16_t BUDDYLIST = 0x21;
        const uint16_t SPAWN_PORTAL = 0x22;
        const uint16_t SERVERMESSAGE = 0x23;
        const uint16_t WARP_TO_MAP = 0x26;
        const uint16_t CHATTEXT = 0x3F;
        const uint16_t MOVE_PLAYER = 0x52;
        const uint16_t FACIAL_EXPRESSION = 0x59;
        const uint16_t CHAR_INFO = 0x1F;
        const uint16_t CHANGE_CHANNEL = 0x09;
        const uint16_t SPAWN_NPC = 0x7B;
        const uint16_t SPAWN_NPC_REQUEST_CONTROLLER = 0x7D;
        const uint16_t NPC_TALK = 0xA0;
        const uint16_t MOVE_MONSTER = 0x6E;
        const uint16_t SPAWN_MONSTER = 0x6A;
        const uint16_t SPAWN_MONSTER_CONTROL = 0x6C;
        const uint16_t KILL_MONSTER = 0x6B;
        const uint16_t MOVE_MONSTER_RESPONSE = 0x6F;
        const uint16_t MODIFY_INVENTORY_ITEM = 0x12;
        const uint16_t UPDATE_STATS = 0x14;
        const uint16_t UPDATE_CHAR_LOOK = 0x5A;
        const uint16_t BOSS_ENV = 0x30;
        const uint16_t MAP_EFFECT = 0x31;
        const uint16_t OX_QUIZ = 0x34;
        const uint16_t GM_EVENT_INSTRUCTIONS = 0x35;
        const uint16_t CLOCK = 0x36;
        const uint16_t SHOW_FOREIGN_EFFECT = 0x5B;
        const uint16_t GIVE_FOREIGN_BUFF = 0x5C;
        const uint16_t DAMAGE_MONSTER = 0x75;
        const uint16_t SHOW_STATUS_INFO = 0x1A;
        const uint16_t UPDATE_QUEST_INFO = 0x1D;
        const uint16_t DROP_ITEM_FROM_MAPOBJECT = 0x83;
        const uint16_t REMOVE_ITEM_FROM_MAP = 0x84;
        const uint16_t OPEN_NPC_SHOP = 0xA3;
        const uint16_t CONFIRM_SHOP_TRANSACTION = 0xA4;
        const uint16_t PARTY_OPERATION = 0x20;
        const uint16_t UPDATE_PARTYMEMBER_HP = 0x5E;
        const uint16_t FAME_RESPONSE = 0x19;
        const uint16_t MULTICHAT = 0x2D;
        const uint16_t WHISPER = 0x2E;
        const uint16_t GIVE_BUFF = 0x15;
        const uint16_t CANCEL_BUFF = 0x16;
        const uint16_t UPDATE_SKILLS = 0x17;
        const uint16_t CLOSE_RANGE_ATTACK = 0x54;
        const uint16_t RANGED_ATTACK = 0x54;
        const uint16_t MAGIC_ATTACK = 0x55;
        const uint16_t DAMAGE_PLAYER = 0x58;
        const uint16_t SHOW_ITEM_EFFECT = 0x60;
        const uint16_t SHOW_CHAIR = 0x61;
        const uint16_t SHOW_ITEM_GAIN_INCHAT = 0x62;
        const uint16_t SUMMON_ATTACK = 0x4D;
        const uint16_t DAMAGE_SUMMON = 0x4E;
        const uint16_t PLAYER_INTERACTION = 0xAE;
        const uint16_t REPORT = 0x1D;
        const uint16_t SPAWN_SPECIAL_MAPOBJECT = 0x4A;
        const uint16_t REMOVE_SPECIAL_MAPOBJECT = 0x4B;
        const uint16_t MOVE_SUMMON = 0x4C;
        const uint16_t MESSENGER = 0xAB;
        const uint16_t OPEN_STORAGE = 0xA7;
        const uint16_t SPAWN_MIST = 0x8C;
        const uint16_t REMOVE_MIST = 0x8D;
        const uint16_t SPAWN_DOOR = 0x90;
        const uint16_t REMOVE_DOOR = 0x91;
        const uint16_t REACTOR_HIT = 0x94;
        const uint16_t REACTOR_SPAWN = 0x96;
        const uint16_t REACTOR_DESTROY = 0x97;
        const uint16_t APPLY_MONSTER_STATUS = 0x71;
        const uint16_t CANCEL_MONSTER_STATUS = 0x72;
        const uint16_t UPDATE_CHAR_BOX = 0x40;

        // PETS
        const uint16_t SPAWN_PET = 0x43;
        const uint16_t MOVE_PET = 0x44;
        const uint16_t PET_CHAT = 0x45;
        const uint16_t PET_NAMECHANGE = 0x46;
        const uint16_t PET_COMMAND = 0x47;


        // Unused Opcodes originally from Odin Rev 988
        //const uint16_t RELOG_RESPONSE = 0xFFFF;
        //const uint16_t SHOW_MONSTER_HP = 0xFFFF;
        //const uint16_t SHOW_QUEST_COMPLETION = 0xFFFF;
        //const uint16_t SHOW_SCROLL_EFFECT = 0xFFFF;
        //const uint16_t CANCEL_FOREIGN_BUFF = 1;
    }
}

#endif

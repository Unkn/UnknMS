/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPBSHOWCHARACTER_H
#define MSPBSHOWCHARACTER_H

#include "MSPacketBuilder.h"
#include "MSCharacter.h"

class MSPBShowCharacter : private MSPacketBuilder
{
    public:
        MSPBShowCharacter(const MSCharacter & character);
        virtual ~MSPBShowCharacter();

        using MSPacketBuilder::getRaw;
};

#endif

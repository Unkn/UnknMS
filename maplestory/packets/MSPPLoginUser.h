/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPPLOGINUSER_H
#define MSPPLOGINUSER_H

#include <memory>
#include <string_view>

#include "HSMConnection.h"

#include "MSPacketProcessor.h"
#include "MSDBAccount.h"

class MSPPLoginUser : public MSPacketProcessor
{
    public:
        MSPPLoginUser(const std::shared_ptr<HSMConnection> & hsmconn, const std::shared_ptr<MSDBAccount> & msdbaccount);
        virtual ~MSPPLoginUser();

        virtual uint16_t getHeader() const;
        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);

    private:
        std::shared_ptr<HSMConnection> hsmconn_;
        std::shared_ptr<MSDBAccount> msdbaccount_;

        bool password_auth(const std::string_view & pass, const MSAuthData & auth_data) const;
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MSPPLoginUser *) -> unique_ptr<MSPPLoginUser, default_delete<MSPPLoginUser> >;
}

#endif

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSHandleLoginPP.h"
#include "MSPacketHeaders.h"
#include "MSPackets.h"
MSHandleLoginPP::MSHandleLoginPP()
{
}

MSHandleLoginPP::~MSHandleLoginPP()
{
}

uint16_t MSHandleLoginPP::getHeader() const
{
    return MSPacketHeaders::v55::HANDLE_LOGIN;
}

bool MSHandleLoginPP::process(
        Buffer &buffIn,
        std::vector<Buffer> & outBuffs,
        MapleClient &client)
{
    // TODO Check the body of this packet to see if there is anything
    // meaningful in it :|
    //
    // TODO In TitanMS behavior varies by player status values (unique to TitanMS?):
    //     1 - new pin
    //     2 - ask for pin
    //     3 - Check pin (setting this status is commented out in my copy of TitanMS)
    //     4 - Looks like this corresponds to a pin match
    auto & loginClient = static_cast<MSLoginClient &>(client);
    bool ret = true;

    switch (loginClient.loginStatus_)
    {
        case MSLoginStatus::NewPin:
            ret = newPIN(buffIn, outBuffs, loginClient);
            break;

        case MSLoginStatus::RequestPin:
            ret = requestPIN(buffIn, outBuffs, loginClient);
            break;

        case MSLoginStatus::CheckPin:
            ret = checkPIN(buffIn, outBuffs, loginClient);
            break;

#if 0
        case MSLoginStatus::LoggedIn:
            ret = loggedIn(buffIn, outBuffs, loginClient);
            break;
#endif

        case MSLoginStatus::None:
        default:
            LOG_DEBUG("Invalid client status {}", static_cast<int>(loginClient.loginStatus_));
            ret = false;
            break;
    }

    return ret;
}

bool MSHandleLoginPP::newPIN(
        Buffer &buffIn __attribute__ ((unused)),
        std::vector<Buffer> & outBuffs,
        MSLoginClient &client __attribute__ ((unused)))
{
    // TODO Determine if htere is any info in this packet we care about. Get
    // more samples...
    //
    //     03 00 01 01 2A 79 49 00     00 00

    auto builder = MSPackets::loginProcess(MSPackets::LoginRsp::RegisterNewPIN);
    outBuffs.emplace_back(std::move(builder.getRaw()));
    return true;
}

bool MSHandleLoginPP::requestPIN(
        Buffer &buffIn __attribute__ ((unused)),
        std::vector<Buffer> & outBuffs,
        MSLoginClient &client __attribute__ ((unused)))
{
    // TODO Determine if there is any data we want to check in the packet
    auto builder = MSPackets::loginProcess(
#ifdef ENABLE_PINS
            MSPackets::LoginRsp::RequestPIN
#else
            MSPackets::LoginRsp::LoginSuccess
#endif
            );
    outBuffs.emplace_back(std::move(builder.getRaw()));
#ifdef ENABLE_PINS
    client.loginStatus_ = MSLoginStatus::CheckPin;
#else
    client.loginStatus_ = MSLoginStatus::LoggedIn;
#endif
    return true;
}

bool MSHandleLoginPP::checkPIN(
        Buffer &buffIn __attribute__ ((unused)),
        std::vector<Buffer> & outBuffs,
        MSLoginClient &client __attribute__ ((unused)))
{
    // TODO Determine if htere is any info in this packet we care about
    //
    //     SUBMIT   03 00     01 00 2A 79 49 00     04 00     31 32 33 34
    //     CHANGE   03 00     02 00 2A 79 49 00     04 00     38 38 38 38
    //                        ^^
    //
    // NOTE the 20 79 49 is the same as the previous packet in newPIN... what is it?

    auto builder = MSPackets::loginProcess(MSPackets::LoginRsp::RequestPIN);
    outBuffs.emplace_back(std::move(builder.getRaw()));

    return true;
}

bool MSHandleLoginPP::loggedIn(
        Buffer &buffIn __attribute__ ((unused)),
        std::vector<Buffer> & outBuffs __attribute__ ((unused)),
        MSLoginClient &client __attribute__ ((unused)))
{
    return false;
}

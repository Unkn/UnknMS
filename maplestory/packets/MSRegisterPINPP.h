/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSREGISTERPINPP_H
#define MSREGISTERPINPP_H

#include "MSPacketProcessor.h"

// TODO Yeah... it really should be MSPPRegisterPIN instead... or snake case :D
class MSRegisterPINPP : public MSPacketProcessor
{
    public:
        MSRegisterPINPP();
        virtual ~MSRegisterPINPP();

        virtual uint16_t getHeader() const;

        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MSRegisterPINPP *) -> unique_ptr<MSRegisterPINPP, default_delete<MSRegisterPINPP> >;
}

#endif

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "Logger.h"
#include "MSPacketHandler.h"

MSPacketHandler::MSPacketHandler()
{
}

MSPacketHandler::~MSPacketHandler()
{
}

bool MSPacketHandler::handle(MapleClient &client)
{
    bool ok = client.readPacket(buffIn_);

    if (ok)
    {
        std::vector<Buffer> outBuffs;
        uint16_t header = *buffIn_.data();
        auto processor = findProcessor(header);
        if (!processor ||
                !processor->process(buffIn_, outBuffs, client) ||
                !sendAllResponsePackets(outBuffs, client))
        {
            LOG_WARN("Failed to handle packet {:04X} for client {:d}", header, client.getConnectionID());
            ok = false;
        }
    }

    return ok;
}

bool MSPacketHandler::registerProcessor(std::unique_ptr<MSPacketProcessor> processor)
{
    assert(processor != nullptr);

    bool ok = true;
    auto header = processor->getHeader();
    auto it = processorMap_.find(header);

    if (it != processorMap_.end())
    {
        LOG_WARN("Attempted to overwite packet processor for header {:04X}", header);
        ok = false;
    }
    else
    {
        processorMap_.emplace(header, std::move(processor));
        LOG_INFO("Added packet processor for header {:04X}", header);
    }

    return ok;
}

MSPacketProcessor *MSPacketHandler::findProcessor(uint16_t header) const
{
    MSPacketProcessor *ret = nullptr;
    auto it = processorMap_.find(header);
    if (it != processorMap_.end())
        ret = it->second.get();
    else
        LOG_WARN("No processor registered for header {:04X}", header);
    return ret;
}

bool MSPacketHandler::sendAllResponsePackets(
        std::vector<Buffer> & outBuffs,
        MapleClient &client)
{
    bool ret = true;
    for (auto & buffOut : outBuffs)
    {
        if (!client.sendPacket(buffOut))
        {
            ret = false;
            break;
        }
    }
    return ret;
}

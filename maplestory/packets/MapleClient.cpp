/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cstring>

#include "Logger.h"
#include "Debug.h"
#include "MSPackets.h"
#include "MSVersion.h"
#include "MapleClient.h"

MapleClient::MapleClient(std::unique_ptr<Socket> && socket) :
    socket_(std::move(socket)),
    should_disconnect_(false)
{
    memset(sendIV_, 0, MSCrypt::IV_LENGTH * sizeof(uint8_t));
    memset(recvIV_, 0, MSCrypt::IV_LENGTH * sizeof(uint8_t));
}

MapleClient::~MapleClient()
{
}

bool MapleClient::establishConnection()
{
    bool ok = true;

    if (!MSCrypt::newIV(sendIV_) || !MSCrypt::newIV(recvIV_))
    {
        LOG_ERROR("Failed to generate IV's for new login client connection {:d}", getConnectionID());
        ok = false;
    }
    else
    {
        auto packet = MSPackets::connection(
                MSVersion::GAME_VERSION,
                MSVersion::GAME_SUBVERSION,
                MSVersion::MAPLE_LOCALE_GLOBAL,
                recvIV_,
                sendIV_);

        ok = sendPacket(packet.getRaw(), false);
    }

    return ok;
}

bool MapleClient::readPacket(Buffer &buff, bool decrypt)
{
    // TODO Just read 4 bytes to get the header then read again for the rest of
    // the body... want to verify this is correct before updating
    uint32_t encHeader;
    size_t headerLen = 4;
    bool ret = socket_->recv(reinterpret_cast<uint8_t *>(&encHeader), headerLen);

    if (!ret)
    {
        LOG_WARN("Failed to read packet size header from client {:d}", getConnectionID());
        // Disconnect the client on a recv failure
        should_disconnect_ = true;
        ret = false;
    }
    else if (headerLen == 0)
    {
        LOG_DEBUG("Read 0 bytes from client {:d}, disconnect?", getConnectionID());
        // It appears that when a client disconnects we will read a 0 length packet.
        should_disconnect_ = true;
        ret = false;
    }
    else
    {
        LOG_DEBUG(">Encrypted Packet: {}", printableBytes(buff.data(), buff.size()));
        if (headerLen < 4)
        {
            LOG_WARN("Packet from client {:d} too short for processing", getConnectionID());
            ret = false;
        }
    }

    if (ret && decrypt)
    {
        // TODO Shoud be able to split this into a left and right header via
        // uint16_t and just xor those 2 halves directly. Verify this and
        // simplify the ops.
        uint16_t packetLen = (encHeader & 0x0000FFFF) ^ (encHeader >> 16);
        LOG_DEBUG("Encrypted header {:08X}, Length {:04X}", encHeader, packetLen);

        buff.resize(packetLen);
        if (!socket_->recv(buff))
        {
            LOG_WARN("Failed to recieve packet body from client {:d}", getConnectionID());
            ret = false;
        }
        else if (buff.size() != packetLen)
        {
            LOG_WARN("Failed to recieve complete packet body from client {:d} ({:d}/{:d})", getConnectionID(), buff.size(), packetLen);
            ret = false;
        }
        else
        {
            MSCrypt::decrypt(buff.data(), packetLen, recvIV_);
            LOG_DEBUG(">Decrypted packet: {}", printableBytes(buff.data(), packetLen));
        }
    }

    return ret;
}

bool MapleClient::sendPacket(Buffer &buff, bool encrypt)
{
    // TODO Clean this shit up
    bool ret = true;

    LOG_DEBUG("<Clear Packet: {}", printableBytes(buff.data(), buff.size()));

    if (encrypt)
    {
        uint8_t sizeHeader[MSCrypt::HEADER_LENGTH];
        MSCrypt::generateHeader(sizeHeader, buff.size(), sendIV_);
#if 0
        ret = socket_->send(sizeHeader, MSCrypt::HEADER_LENGTH);
        if (!ret)
        {
            LOG_WARN("Failed to send size header to client {:d}", getConnectionID());
            ret = false;
        }
        else
        {
#else
            MSCrypt::encrypt(buff.data(), buff.size(), sendIV_);
            LOG_DEBUG(">Encrypted packet: {}", printableBytes(buff.data(), buff.size()));

            // TODO If this works, then instead of memmove every time, reserve
            // header length bytes before the packet
            size_t oldSize = buff.size();
            buff.resize(oldSize + MSCrypt::HEADER_LENGTH);
            memmove(buff.data() + MSCrypt::HEADER_LENGTH, buff.data(), oldSize);
            memcpy(buff.data(), sizeHeader, MSCrypt::HEADER_LENGTH);
#endif
        //}
    }

    if (ret)
    {
        ret = socket_->send(buff);
        if (!ret)
        {
            LOG_WARN("Failed to send packet to client {:d}", getConnectionID());
            ret = false;
        }
    }

    return ret;
}

int MapleClient::getConnectionID() const
{
    return -1; // everybody is -1 for now :|
}

bool MapleClient::should_disconnect() const
{
    return should_disconnect_;
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPPSELECTCHANNEL_H
#define MSPPSELECTCHANNEL_H

#include "MSPacketProcessor.h"
#include "MSPacketBuilder.h"
#include "MSCharacter.h"
#include "MSDBCharacter.h"

class MSPPSelectChannel : public MSPacketProcessor
{
    public:
        // TODO inject method to get players info from DB or chache
        MSPPSelectChannel(const std::shared_ptr<MSDBCharacter> & msdb_char);
        virtual ~MSPPSelectChannel();

        virtual uint16_t getHeader() const;
        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);

    private:
        std::shared_ptr<MSDBCharacter> msdb_char_;
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MSPPSelectChannel *) -> unique_ptr<MSPPSelectChannel, default_delete<MSPPSelectChannel> >;
}

#endif

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPBShowCharacter.h"

#include "MSPacketHeaders.h"
#include "MSPBData.h"

MSPBShowCharacter::MSPBShowCharacter(const MSCharacter & character)
{
    add(MSPacketHeaders::v55::SHOW_CHARACTER);
    add<uint8_t>(0); // TODO What is this
    add<MSCharacterDisplay>(character);
    // NOTE TitanMS had an addition 8 zero bytes at the end of this packet but
    // not sure why yet (6 mystery zero ints instead of 4 like in the channel
    // select packet code per character). Character select doesnt seems to have
    // a problem (yet) without those additional bytes in the response packet.
}

MSPBShowCharacter::~MSPBShowCharacter()
{
}

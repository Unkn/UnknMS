/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPBData.h"

template<>
MSPacketBuilder & MSPacketBuilder::add(const MSCharacter & c)
{
    // Example packet data from TitanMS
    //
    // 01 00 00 00 char id
    // 66 75 63 6B 00 00 00 00 00 00 00 00 name
    // 00 ?
    // 01 gender
    // 02 skin
    // 04 00 00 00 eyes
    // 05 00 00 00 hair
    // 00 00 00 00 ?
    // 00 00 00 00 ?
    // 00 00 00 00 ?
    // 00 00 00 00 ?
    // 00 00 00 00 ?
    // 00 00 00 00 ?
    // 2D level
    // 02 00 job
    // 7B 00 str
    // 41 01 dex
    // D5 00 int
    // 38 01 luk
    // E8 03 cur hp
    // D0 07 max hp
    // B8 0B cur mp
    // A0 0F map mp
    // 07 00 ap
    // 08 00 sp
    // D2 04 00 00 exp
    // 39 05 fame
    // 00 00 00 00 map
    // 01 pos

    add(c.dbid_);

    // TODO it looks like a max len of 12 is supported and any bytes that
    // follow need to be 0? See TitanMS LoginPacket::showCharacters.
    add(c.name_.data(), c.name_.size());
    for (auto i = c.name_.size(); i < 12; ++i)
        add<uint8_t>(0);

    add<uint8_t>(0); // TODO Whats this for? Guaranteed string null terminator?

    add(c.gender_);
    add(c.skin_);
    add(c.face_);
    add(c.hair_);

    // TODO What are these for?
    for (int i = 0; i < 6; ++i)
        add<uint32_t>(0);

    add(c.level_);
    add(c.job_);

    add(c.str_);
    add(c.dex_);
    add(c.int_);
    add(c.luk_);

    add(c.curHP_);
    add(c.maxHP_);
    add(c.curMP_);
    add(c.maxMP_);

    add(c.ap_);
    add(c.sp_);

    add(c.exp_);
    add(c.fame_);
    add(c.map_);
    add(c.pos_);

    return *this;
}

template<>
MSPacketBuilder & MSPacketBuilder::add(const MSCharacterDisplay & cd)
{
    auto & c = *cd;

    // NOTE: If this display is ever needed separately then we can update the
    // code using this function to also do a call on the MSCharacter directly
    add(c);

    // Example packet data following the MSCharacter packet data from TitanMS
    //
    // 01 gender
    // 02 skin
    // 04 00 00 00 eyes
    // 01 ?
    // 05 00 00 00 hair
    // FF FF end equip list
    // 00 00 00 00 ?
    // 00 00 00 00 ?
    // 00 00 00 00 ?
    // 00 00 00 00 ?
    // 00

    add(c.gender_);
    add(c.skin_);
    add(c.face_);

    add<uint8_t>(1); // TODO TitanMS literally marks this as "???"...

    add(c.hair_);

    for (const auto & equip : c.equips_)
    {
        add(equip.type_);
        add(equip.id_);
    }

    // TODO seems like it HAS to be a list terminator :P
    add<uint16_t>(-1); // probably just put 0xFF directly

    // TODO What are these for?
    for (int i = 0; i < 4; ++i)
        add<uint32_t>(0);

    add<uint8_t>(0); // TODO Whats this for?

    return *this;
}

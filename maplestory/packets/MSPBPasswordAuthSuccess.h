/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPBPASSWORDAUTHSUCCESS_H
#define MSPBPASSWORDAUTHSUCCESS_H

#include "MSPacketBuilder.h"

#include "MSAccount.h"

class MSPBPasswordAuthSuccess : public MSPacketBuilder
{
    public:
        MSPBPasswordAuthSuccess(const MSAccount & account);
        virtual ~MSPBPasswordAuthSuccess();

        using MSPacketBuilder::getRaw;
};

#endif

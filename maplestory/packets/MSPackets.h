/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPACKETS_H
#define MSPACKETS_H

#include "MSPacketBuilder.h"
#include "MSCrypt.h"

namespace MSPackets
{
    MSPacketBuilder connection(uint16_t gameVersion, const std::string & subVersion, uint8_t gameLocale, uint8_t recvIV[MSCrypt::IV_LENGTH], uint8_t sendIV[MSCrypt::IV_LENGTH]);

    enum class LoginRsp : uint8_t
    {
        LoginSuccess = 0x00,    // Client should respond with 0x18 to show the worlds
        RegisterNewPIN = 0x01,  // Client will display a set new pin dialog
        InvalidPIN = 0x02,      // Client will display a pin rejection error
        // 0x03 causes a notification of a error in client
        RequestPIN = 0x04       // Client will display a pin request dialog
    };

    MSPacketBuilder loginProcess(LoginRsp rsp);
}

#endif

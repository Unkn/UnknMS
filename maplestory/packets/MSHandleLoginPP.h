/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSHANDLELOGINPP_H
#define MSHANDLELOGINPP_H

#include <memory>

#include "MSPacketProcessor.h"
#include "MSLoginClient.h"

class MSHandleLoginPP : public MSPacketProcessor
{
    public:
        MSHandleLoginPP();
        virtual ~MSHandleLoginPP();

        virtual uint16_t getHeader() const;

        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);

    private:
        bool newPIN(Buffer &buffIn, std::vector<Buffer> & outBuffs, MSLoginClient &client);
        bool requestPIN(Buffer &buffIn, std::vector<Buffer> & outBuffs, MSLoginClient &client);
        bool checkPIN(Buffer &buffIn, std::vector<Buffer> & outBuffs, MSLoginClient &client);
        bool loggedIn(Buffer &buffIn, std::vector<Buffer> & outBuffs, MSLoginClient &client);
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MSHandleLoginPP *) -> unique_ptr<MSHandleLoginPP, default_delete<MSHandleLoginPP> >;
}

#endif

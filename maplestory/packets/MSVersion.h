/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSVERSION_H
#define MSVERSION_H

#include <cstdint>
#include <string>

namespace MSVersion
{
    // TODO Needs to be command line parameters
    const uint16_t GAME_VERSION = 55;
    const std::string GAME_SUBVERSION = "";

    // Normal servers
    const uint8_t MAPLE_LOCALE_GLOBAL = 0x08;
    const uint8_t MAPLE_LOCALE_KOREA = 0x01;
    const uint8_t MAPLE_LOCALE_JAPAN = 0x03;
    const uint8_t MAPLE_LOCALE_CHINA = 0x05;
    const uint8_t MAPLE_LOCALE_TAIWAN = 0x06;
    const uint8_t MAPLE_LOCALE_SEA = 0x07;
    const uint8_t MAPLE_LOCALE_THAILAND = 0x07;
    const uint8_t MAPLE_LOCALE_EUROPE = 0x09;
    const uint8_t MAPLE_LOCALE_BRAZIL = 0x09;

    // Test servers
    const uint8_t MAPLE_LOCALE_KOREA_TEST = 0x02;
    const uint8_t MAPLE_LOCALE_CHINA_TEST = 0x04;
    const uint8_t MAPLE_LOCALE_TAIWAN_TEST = 0x06;
    const uint8_t MAPLE_LOCALE_GLOBAL_TEST = 0x05;
}

#endif

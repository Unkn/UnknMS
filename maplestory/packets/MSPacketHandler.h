/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPACKETHANDLER_H
#define MSPACKETHANDLER_H

#include <map>
#include <memory>

#include "buffer.hpp"
#include "MapleClient.h"
#include "MSPacketProcessor.h"

// ClientReqHandler?
class MSPacketHandler
{
    public:
        MSPacketHandler();
        virtual ~MSPacketHandler();

        bool handle(MapleClient &client);

        bool registerProcessor(std::unique_ptr<MSPacketProcessor> processor);

    private:
        // Reuse the read buffer. MIGHT need to make this per connections.
        Buffer buffIn_;
        // TODO This may need to change depending on the variance on what
        // information each packet type needs for processing
        // TODO We really dont need a full map, just a tree with find based on
        // header type.  Make a specialized one later (or same thing but with a
        // hash map if we can get non-negligible lookup performance increase
        // from it...  might not since 256 keys max)
        std::map<uint16_t, std::unique_ptr<MSPacketProcessor> > processorMap_;

        MSPacketProcessor *findProcessor(uint16_t header) const;

        bool sendAllResponsePackets(std::vector<Buffer> & outBuffs, MapleClient &client);
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MSPacketHandler *) -> unique_ptr<MSPacketHandler, default_delete<MSPacketHandler> >;
}

#endif

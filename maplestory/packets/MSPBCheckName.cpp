/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPBCheckName.h"

#include <type_traits>

#include "MSPacketHeaders.h"

MSPBCheckName::MSPBCheckName(
        bool available,
        const std::string_view & name)
{
    add(MSPacketHeaders::v55::CHECK_NAME_RSP);
    add(name);
    // Appears that the client will take any non-zero response as unavailable
    add<uint8_t>(available ? 0 : 1);
}

MSPBCheckName::~MSPBCheckName()
{
    // Pineapples
}

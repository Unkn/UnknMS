/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "portable_endian.h"
#include "MSPacketProcessor.h"

bool Packet::get(PacketSegment & seg, size_t len)
{
    bool ok = sufficentDataAvailable(len);
    if (ok)
    {
        seg.data_ = buff_.data() + pos_;
        seg.len_ = len;
        pos_ += len;
    }
    return ok;
}

bool Packet::get(uint8_t * out, size_t sizeofOut)
{
    bool ok = sufficentDataAvailable(sizeofOut);
    if (ok)
    {
        uint8_t *data = buff_.data() + pos_;
        switch (sizeofOut)
        {
            case 1:
                *out = *data;
                break;

            case 2:
            {
                *reinterpret_cast<uint16_t *>(out) =
                    le16toh(*reinterpret_cast<uint16_t *>(data));
                break;
            }

            case 4:
            {
                *reinterpret_cast<uint32_t *>(out) =
                    le32toh(*reinterpret_cast<uint32_t *>(data));
                break;
            }

            case 8:
            {
                *reinterpret_cast<uint64_t *>(out) =
                    le64toh(*reinterpret_cast<uint64_t *>(data));
                break;
            }

            default:
                assert(!"Unsupported default input for default template for packet builder add");
                ok = false;
                break;
        }

        if (ok)
            pos_ += sizeofOut;
    }
    return ok;
}

template<>
bool Packet::get(std::string_view & seg)
{
    uint16_t len;
    bool ok = get(len) && sufficentDataAvailable(len);
    if (ok)
    {
        // TODO Check that all characters are valid (all_of isprint?)
        seg = std::string_view(reinterpret_cast<char *>(buff_.data() + pos_), len);
        pos_ += len;
    }
    return ok;
}

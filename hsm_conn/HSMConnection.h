/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef HSMCONNECTION_H
#define HSMCONNECTION_H

#include "buffer.hpp"
#include "Socket.h"

#define SHA256_DIGEST_LEN 32

class HSMConnection
{
    public:
        HSMConnection(std::unique_ptr<Socket> socket);
        virtual ~HSMConnection();

        virtual bool hmac_sha256(const uint8_t * input, size_t len, uint8_t hmac[SHA256_DIGEST_LEN]);

    private:
        std::unique_ptr<Socket> socket_;
};

#endif

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <algorithm>
#include <arpa/inet.h>

#include "HSMConnection.h"
#include "Zeroize.h"
#include "Logger.h"

// TODO Get rid of this garbage when we clean up the hsm packet encoding
#define HMAC_SHA256 0x01
#define HMAC_SHA256_PACKET_LEN(x)   (sizeof(uint16_t) + sizeof(uint32_t) + x)
#define HMAC_SHA256_HEADER_OFF(x)   (x)
#define HMAC_SHA256_SIZE_OFF(x)     (HMAC_SHA256_HEADER_OFF(x) + sizeof(uint16_t))
#define HMAC_SHA256_DATA_OFF(x)     (HMAC_SHA256_SIZE_OFF(x) + sizeof(uint32_t))
#define HMAC_SHA256_RSP_LEN         (sizeof(uint16_t) + SHA256_DIGEST_LEN)

HSMConnection::HSMConnection(std::unique_ptr<Socket> socket) :
    socket_(std::move(socket))
{
}

HSMConnection::~HSMConnection()
{
}

bool HSMConnection::hmac_sha256(const uint8_t * input, size_t len, uint8_t hmac[SHA256_DIGEST_LEN])
{
    bool ret = false;
    uint16_t header = htons(HMAC_SHA256);
    uint32_t packet_len = HMAC_SHA256_PACKET_LEN(len);
    uint32_t packet_len_n = htonl(packet_len);
    // We can reuse the buffer for reading in the response
    auto packet = std::make_unique<uint8_t []>(
            std::max(packet_len, static_cast<uint32_t>(HMAC_SHA256_RSP_LEN)));
    auto raw = packet.get();
    size_t recv_len;

    *reinterpret_cast<uint16_t *>(HMAC_SHA256_HEADER_OFF(raw)) = header;
    *reinterpret_cast<uint32_t *>(HMAC_SHA256_SIZE_OFF(raw)) = htonl(len);
    memcpy(HMAC_SHA256_DATA_OFF(raw), input, len);

    if (!socket_->send(reinterpret_cast<uint8_t *>(&packet_len_n), sizeof(packet_len_n)))
        LOG_ERROR("Failed to send packet length to HSM");
    else if (!socket_->send(raw, packet_len))
        LOG_ERROR("Failed to send packet to HSM");
    else if (!socket_->recv(reinterpret_cast<uint8_t *>(&packet_len_n), (recv_len = sizeof(packet_len_n))))
        LOG_ERROR("Failed to retrieve packet length from HSM");
    else if (!socket_->recv(raw, (recv_len = ntohl(packet_len_n))) || recv_len != HMAC_SHA256_RSP_LEN)
        LOG_ERROR("Failed to retrieve packet from HSM");
    else if (*reinterpret_cast<uint16_t *>(raw) != header)
        LOG_ERROR("Packet header mismatch {:04X}", ntohs(*reinterpret_cast<uint16_t *>(raw)));
    else
    {
        ret = true;
        memcpy(hmac, raw + sizeof(uint16_t), SHA256_DIGEST_LEN);
    }

    // Assuming the input buffer contains a password so we want to zeroize it
    zeroize(raw, HMAC_SHA256_PACKET_LEN(len));

    return ret;
}

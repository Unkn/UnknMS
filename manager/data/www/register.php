<?php

include "dbconn.php";

define("SALT_LEN",          32);
define("SHA256_HMAC_LEN",   32);
define("HSM_SHA256_HMAC",   0x0001);
define("HSM_RAND",          0x0002);

$register_errors = array();
$register_internal_error = false;
$register_success = false;

function hsm_send($socket, $req) {
    $req_len = pack("N", strlen($req));
    // TODO Check for socket errors
    socket_write($socket, $req_len, 4);
    socket_write($socket, $req, strlen($req));

    return true;
}

function hsm_recv($socket, &$rsp) {
    global $register_internal_error;

    // TODO Check that for socket errors and that we in fact read exactly how much we requested
    $rsp_len = socket_read($socket, 4, PHP_BINARY_READ);
    if ($rsp_len === false) {
        syslog(LOG_ERR, "Web server failed to get response from hsm");
        $register_internal_error = true;
        return false;
    }

    $rsp_len = unpack("Nlen", $rsp_len);
    $rsp = socket_read($socket, $rsp_len["len"], PHP_BINARY_READ);

    return true;
}

function hsm_transaction($socket, $req, &$rsp) {
    return hsm_send($socket, $req) && hsm_recv($socket, $rsp);
}

function get_new_salt($socket, &$pass_salt) {
    $req = pack("nN", HSM_RAND, SALT_LEN);
    $rsp = "";
    if (hsm_transaction($socket, $req, $rsp)) {
        // TODO Check length
        $pass_salt = substr($rsp, 2);
        return true;
    } else {
        return false;
    }
}

function get_password_hmac($socket, $pass, $pass_salt, &$pass_hmac) {
    $hmac_input = $pass . $pass_salt;
    $req = pack("nN", HSM_SHA256_HMAC, strlen($hmac_input)) . $hmac_input;
    $rsp = "";
    if (hsm_transaction($socket, $req, $rsp)) {
        // TODO Check length
        $pass_hmac = substr($rsp, 2);
        return true;
    } else {
        return false;
    }
}

function new_pass_salt_hmac($pass, &$pass_hmac, &$pass_salt) {
    // TODO Upgrade this to a TLS connection
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($socket === false) {
        syslog(LOG_ERR, "socket_create() failed: " . socket_strerror(socket_last_error()));
        $register_internal_error = true;
        return false;
    }

    $pass_salt = "";
    $pass_hmac = "";
    $result = false;

    if (socket_set_block($socket) === false) {
        syslog(LOG_ERR, "Failed to set socket blocking");
    }
    // TODO Update the ip address when this is on a separate device
    else if (!socket_connect($socket, "localhost", 9999)) {
        syslog(LOG_ERR, "socket_connect() failed: " . socket_strerror(socket_last_error()));
    } else if (!get_new_salt($socket, $pass_salt)) {
        syslog(LOG_ERR, "Failed to get new salt from HSM");
    } else if (!get_password_hmac($socket, $pass, $pass_salt, $pass_hmac)) {
        syslog(LOG_ERR, "Failed to get HMAC from HSM for new user password");
    } else {
        $result = true;
    }

    socket_close($socket);
    return $result;
}

function register_new_user($dbconn, $username, $email, $dob, $pass_hmac, $pass_salt) {
    // Prepare and bind
    $stmt = $dbconn->prepare("INSERT INTO accounts (name, pass, salt, email, dob) VALUES (?, ?, ?, ?, ?)");
    if (mysqli_connect_errno()) {
        syslog(LOG_ERR, "Prepare failed: " . mysqli_connect_errno());
        return false;
    }

    // pass and hmac are blobs but strings... dunno... php pls
    $stmt->bind_param("sssss", $username, $pass_hmac, $pass_salt, $email, $dob);
    $result = $stmt->execute();
    if (!$result) {
        syslog(LOG_ERR, "Execute failed: " . $stmt->error);
    }

    $stmt->close();
    return $result;
}

function username_available($dbconn, $name) {
    global $register_errors;
    global $register_error_internal;

    $stmt = $dbconn->prepare("SELECT 1 FROM accounts WHERE name=? LIMIT 1");
    if (mysqli_connect_errno()) {
        $register_error_internal = true;
        syslog(LOG_ERR, "Failed to prepare statement to check name availability: " . mysqli_connect_errno());
        return false;
    }

    $stmt->bind_param("s", $name);
    $ok = false;
    if (!$stmt->execute()) {
        // TODO should this ever fail? find out the failure cases. Just mark it
        // as an internal error until otherwise determined not to be...
        $register_error_internal = true;
        syslog(LOG_ERR, "Failed to prepare statement to check name availability: " . mysqli_connect_errno());
    } else if ($stmt->get_result()->num_rows > 0) {
        array_push($register_errors, "Username unavailable");
    } else {
        $ok = true;
    }

    $stmt->close();
    return $ok;
}

function validate_name($dbconn, $name) {
    global $register_errors;

    $ok = false;
    $name_len = strlen($name);

    if ($name_len === 0) {
        array_push($register_errors, "Username is a required field");
    } else if ($name_len < 4 || 20 < $name_len || !ctype_alnum($name)) {
        array_push($register_errors, "Username must be between 4 and 20 alphanumeric characters");
    } else if (username_available($dbconn, $name)) {
        $ok = true;
    }

    return $ok;
}

function validate_password($pass, $confirm) {
    global $register_errors;

    $ok = false;
    if (strcmp($pass, $confirm) != 0) {
        array_push($register_errors, "Passwords do not match");
    } else {
        // TODO find out password limitations for maplestory
        $ok = true;
    }

    return $ok;
}

function validate_email($email) {
    // TODO Actually validate it :|
    // https://en.wikipedia.org/wiki/Email_address#Syntax
    return true;
}

function is_13_or_older($year, $month, $day) {
    $today = getdate();
    $t_year = $today["year"];
    $t_month = $today["month"];
    $t_day = $today["day"];

    if ($year + 13 > $t_year) {
        return false;
    } else if ($year + 13 == $t_year) {
        if ($month > $t_month) {
            return false;
        } else if ($month == $t_month && $day > $t_day) {
            return false;
        }
    }
    return true;
}

function validate_dob($dob) {
    global $register_errors;

    $ok = false;
    $date = explode("-", $dob);
    if (strlen($dob) == 0) {
        array_push($register_errors, "Date of birth required");
    } else if (sizeof($date) != 3 || !checkdate($date[2], $date[1], $date[0])) {
        array_push($register_errors, "Invalid date");
    }
    // May as well just do it becuase why not ¯\_(ツ)_/¯
    // https://en.wikipedia.org/wiki/Children%27s_Online_Privacy_Protection_Act
    //
    // When is next season of silicon valley... D:
    else if (!is_13_or_older((int)$date[0], (int)$date[1], (int)$date[2])) {
        array_push($register_errors, "Must be 13 or older to register");
    } else {
        $ok = true;
    }

    return $ok;
}

if (isset($_POST['btn-register'])) {
    global $register_internal_error;

    $dbconn = login_db_conn();
    if (mysqli_connect_errno()) {
        syslog(LOG_ERR, "Failed to connect to database for user registration: " . mysqli_connect_errno());
        $register_internal_error = true;
    } else {
        $name=$_POST['id'];
        $email=$_POST['email'];
        $dob=$_POST['dob'];
        $pass=$_POST['password'];
        $confirm=$_POST['confirm'];

        // Get as many errors as possible for display when invalid input
        $valid = validate_name($dbconn, $name);
        $valid = validate_password($pass, $confirm) && $valid;
        $valid = validate_email($email) && valid;
        $valid = validate_dob($dob) && valid;
        if ($valid) {
            $pass_salt='';
            $pass_hmac='';

            if (new_pass_salt_hmac($pass, $pass_hmac, $pass_salt) &&
                register_new_user($dbconn, $name, $email, $dob, $pass_hmac, $pass_salt)) {
                $register_success = true;
            } else {
                $register_internal_error = true;
            }
        }

        $dbconn->close();
    }
}

?>

<!DOCTYPE html>
<html>
    <head>
        <script type="application/javascript" src="register.js"></script>
        <link rel="stylesheet" href="register.css">
    </head>
    <body>
        <?php if ($register_success): ?>
        register success...
        <?php else: ?>
        <form id="register" action="/register.php" method="post">
            <?php
                if ($register_internal_error) {
                    echo '<ul class="form-errors"><li class="form-error">';
                    echo 'Internal error: contact support';
                    echo '</li></ul>';
                } else if (count($register_errors) > 0) {
                    echo '<ul class="form-errors">';
                    foreach ($register_errors as $value) {
                        echo '<li class="form-error">';
                        echo $value;
                        echo '</li>';
                    }
                    echo '</ul>';
                }
            ?>
            <div class="form-input">
                <label>Username</label>
                <input type="text" name="id" id="id" class="input">
            </div>
            <div class="form-input">
                <label>Email</label>
                <input type="text" name="email" id="email" class="input">
            </div>
            <div class="form-input">
                <label>Date of Birth</label>
                <input type="date" name="dob" id="dob" class="input">
            </div>
            <div class="form-input">
                <label>Password</label>
                <input type="password" name="password" id="password" class="input">
            </div>
            <div class="form-input">
                <label>Confirm Password</label>
                <input type="password" name="confirm" id="confirm" class="input">
            </div>
            <div class="form-input">
                <input type="submit" name="btn-register" value="Register" class="submit">
            </div>
        </form>
        <?php endif; ?>
    </body>
</html>

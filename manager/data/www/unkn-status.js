function LogInfo() {
    this.lines=0;
    this.maxlines=50;
    // TODO Figure this out so we can have dynamic limit. For now we just do
    // the remove last line regex.
    //this.matcher = new RegExp();
}

function addLogRow(source, date, monotonic, level, message) {
    var tbl=document.getElementById('logTable');
    var new_row=tbl.rows[1].cloneNode(true);
    new_row.cells[0].innerHTML=source;
    new_row.cells[1].innerHTML=date;
    new_row.cells[2].innerHTML=monotonic;
    new_row.cells[3].innerHTML=level;
    new_row.cells[4].innerHTML=message;
    new_row.style.display='';
    tbl.appendChild(new_row);
}

function doConnect() {
    socket = new WebSocket("ws://localhost:10000", "log");
    li = new LogInfo();

    socket.onopen = function (e) { alert("opened " + uri); };
    socket.onclose = function (e) { alert("closed"); socket = null; };
    socket.onmessage = function (e) {
        var ta = document.getElementById('logs');
        if (li.lines >= li.maxlines) {
            ta.value=ta.value.replace(/\r?\n?[^\r\n]*$/, "");
        } else {
            li.lines = li.lines + 1;
        }
        ta.value=e.data + '\n' + ta.value;

//        var source_end=e.data.indexOf('|');
//        var date_end=e.data.indexOf('|', source_end+1);
//        var monotonic_end=e.data.indexOf('|', date_end+1);
//        var level_end=e.data.indexOf('|', monotonic_end+1);
//
//        // TODO Need to handle the cases where there is supposed to be a 0
//        // length substring
//        addLogRow(
//            e.data.substring(0, source_end),
//            e.data.substring(source_end + 1, date_end),
//            e.data.substring(date_end + 1, monotonic_end),
//            e.data.substring(monotonic_end + 1, level_end),
//            e.data.substring(level_end + 1));

    };
    socket.onerror = function (e) { if (e.data) alert("Error: " + e.data); };
}

function doSendInput() {
    if (socket == null) {
        alert("socket is not connected");
        return;
    }

    socket.send("potato");
}

function doDisconnect() {
    socket.close();
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <algorithm>

#include "libwebsockets.h"

//******************************************************************************
//******************************************************************************
// Begin extra ghetto test code...
// TODO Clean it up or find another way...
//******************************************************************************
//******************************************************************************

#include <map>
#include <vector>

#define DEFAULT_MAX_QUEUED_LOGS 50

class Log
{
    public:
        Log() :
            last_(0),
            total_(0),
            queued_(0),
            max_(DEFAULT_MAX_QUEUED_LOGS) // TODO configurable at runtime
        {
            cachedLogs_.reserve(max_);
        }

        void queueMessage(const std::string & s)
        {
            // Not filled up yet, dont worry about rotating
            if (queued_ < max_)
            {
                cachedLogs_.emplace_back(s);
                ++queued_;
            }
            else
            {
                cachedLogs_[last_] = s;
                if (last_ + 1 == max_)
                    last_ = 0;
                else
                    ++last_;
            }
            ++total_;
        }

        const std::string & getMessage(struct lws * wsi)
        {
            const std::string * ret = &EMPTY;
            if (queued_)
            {
                auto it = lastClientLog_.find(wsi);
                if (it == lastClientLog_.end())
                {
                    // Let the new client pick up all the cached messages
                    ClientData data = { total_, last_ };
                    lastClientLog_.emplace(wsi, data);
                    ret = &cachedLogs_[data.lastIndex_];
                }
                else if (it->second.lastTotal_ != total_ ||
                        it->second.lastIndex_ != (last_ + queued_ - 1) % max_)
                {
                    auto & data = it->second;

                    data.lastIndex_ = (total_ - data.lastTotal_ < queued_) ?
                        ((data.lastIndex_ + 1) % max_) :
                        last_;
                    data.lastTotal_ = total_;
                    ret = &cachedLogs_[data.lastIndex_];
                }
            }
            return *ret;
        }

    private:
        static const std::string EMPTY;

        size_t last_;
        size_t total_;
        size_t queued_;
        size_t max_;

        struct ClientData
        {
            size_t lastTotal_;
            size_t lastIndex_;
        };

        std::map<struct lws *, ClientData> lastClientLog_;
        std::vector<std::string> cachedLogs_;
} logTest;

const std::string Log::EMPTY;

//******************************************************************************
//******************************************************************************
// end...
//******************************************************************************
//******************************************************************************

static int callback_http(
        struct lws * wsi __attribute__ ((unused)),
        enum lws_callback_reasons reason __attribute__ ((unused)),
        void * user __attribute__ ((unused)),
        void * in __attribute__ ((unused)),
        size_t len __attribute__ ((unused)))
{
    switch (reason)
    {
        case LWS_CALLBACK_GET_THREAD_ID:
        case LWS_CALLBACK_CHANGE_MODE_POLL_FD:
        case LWS_CALLBACK_LOCK_POLL:
        case LWS_CALLBACK_UNLOCK_POLL:
            // Ignored
            break;

        default:
            printf("callback_http unhandled reason %d\n", (int)reason);
            break;
    }
    return 0;
}

static int callback_web_log(
        struct lws * wsi __attribute__ ((unused)),
        enum lws_callback_reasons reason,
        void * user __attribute__ ((unused)),
        void * in,
        size_t len __attribute__ ((unused)))
{
    switch (reason)
    {
        case LWS_CALLBACK_ESTABLISHED:
            printf("connection established\n");
            break;

        case LWS_CALLBACK_RECEIVE:
            printf("recieved: %s\n", static_cast<char *>(in));
            break;

        case LWS_CALLBACK_SERVER_WRITEABLE:
            {
                const std::string & s = logTest.getMessage(wsi);

                if (!s.empty())
                {
                    unsigned char buffer[4096 + LWS_PRE];
                    unsigned char * p = buffer + LWS_PRE;
                    size_t len = std::min((size_t)4095,s.size());
                    strncpy((char *)p, s.c_str(), 4096);
                    lws_write(wsi, p, len, LWS_WRITE_TEXT);
                }
            }
            break;

        default:
            printf("callback_web_log reason %d\n", (int)reason);
            break;
    }

    return 0;
}

static int callback_get_logs(
        struct lws * wsi __attribute__ ((unused)),
        enum lws_callback_reasons reason,
        void * user __attribute__ ((unused)),
        void * in,
        size_t len __attribute__ ((unused)))
{
    switch (reason)
    {
        case LWS_CALLBACK_ESTABLISHED:
            printf("callback_get_logs connection established\n");
            break;

        case LWS_CALLBACK_RECEIVE:
            printf("recieved: %s\n", static_cast<char *>(in));
            logTest.queueMessage(static_cast<char *>(in));
            break;

        default:
            printf("callback_get_logs reason %d\n", (int)reason);
            break;
    }

    return 0;
}

static struct lws_protocols protocols[] =
{
    // First protocol required to be HTTP handler
    {
        "http-only",
        callback_http,
        0, 0, 0, nullptr, 0
    },
    {
        "log",
        callback_web_log,
        0, 0, 0, nullptr, 0
    },
    {
        "log-source",
        callback_get_logs,
        0, 0, 0, nullptr, 0
    },
    {
        nullptr, nullptr, 0, 0, 0, nullptr, 0
    }
};

int main(int argc __attribute__ ((unused)), char ** argv __attribute__ ((unused)))
{
    unsigned int ms = 0;
    unsigned int oldms = 0;
    struct lws_context_creation_info info;
    memset(&info, 0, sizeof(info));

    info.port = 10000;
    info.protocols = protocols;

    lws_context * ctx = lws_create_context(&info);
    if (!ctx)
    {
        fprintf(stderr, "libwebsocket init error\n");
        return -1;
    }

    printf("Starting server\n");

    while (true)
    {
		struct timeval tv;
		gettimeofday(&tv, NULL);
		ms = (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
		if ((ms - oldms) > 9)
        {
            lws_callback_on_writable_all_protocol(ctx, &protocols[1]);
			oldms = ms;
		}

        lws_service(ctx, 10);
    }

    lws_context_destroy(ctx);

    return 0;
}

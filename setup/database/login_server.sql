DROP DATABASE IF EXISTS login_server;
CREATE DATABASE login_server;
USE login_server;

CREATE TABLE accounts (
    id          INT     NOT NULL AUTO_INCREMENT,
    -- TODO Find the actual limits on these that are supported by the v55 client protocol
    name        VARCHAR(20) NOT NULL UNIQUE,
    -- TODO Rename pass to hmac
    pass        BINARY(32)  NOT NULL COMMENT 'Size is for a SHA256 hmac',
    salt        BINARY(32)  NOT NULL COMMENT 'Salt to append to password before peforming the HMAC',
    -- TODO ISO 9564 pin block because why not lol it really is going to be
    -- unnecessary though because the client end is compromised and the pass
    -- should be sufficient. Project is all about playin with random shit tho :P
    pin         CHAR(4)     COMMENT 'PIN used after password authentication',
    created     DATETIME    DEFAULT CURRENT_TIMESTAMP,
    last_access DATETIME    DEFAULT CURRENT_TIMESTAMP,
    email       TEXT        NOT NULL,
    dob         DATE        NOT NULL,
    -- TODO Gender, see MSPBPasswordAuthSuccess
    user_type   TINYINT     NOT NULL DEFAULT 0,

    -- TODO Index the name column
    PRIMARY KEY (id)
);

-- TODO Consider splitting this out into a separate DB belonging to a specific
-- world. Probably a bad idea though. Make the db connection configurable to
-- allow the option of having the db on a separate dedicated server.
-- TODO Look into live backups
-- https://dev.mysql.com/doc/refman/5.7/en/replication-solutions-backups.html
CREATE TABLE characters (
    id          INT NOT NULL AUTO_INCREMENT,
    level       TINYINT UNSIGNED NOT NULL DEFAULT 1,

    -- TODO Default job beginner
    -- TODO Go through all these and determine which ones are signed and
    -- unsigned in the client
    job         SMALLINT NOT NULL,
    str         SMALLINT UNSIGNED NOT NULL,
    dex         SMALLINT UNSIGNED NOT NULL,
    -- int reserved :P
    intel       SMALLINT UNSIGNED NOT NULL,
    luk         SMALLINT UNSIGNED NOT NULL,
    cur_hp      SMALLINT UNSIGNED NOT NULL,
    max_hp      SMALLINT UNSIGNED NOT NULL,
    cur_mp      SMALLINT UNSIGNED NOT NULL,
    max_mp      SMALLINT UNSIGNED NOT NULL,

    ap          SMALLINT UNSIGNED NOT NULL DEFAULT 0,
    sp          SMALLINT UNSIGNED NOT NULL DEFAULT 0,
    exp         INT UNSIGNED NOT NULL DEFAULT 0,
    mesos       INT UNSIGNED NOT NULL DEFAULT 0,
    fame        SMALLINT UNSIGNED NOT NULL DEFAULT 0,

    gender      TINYINT NOT NULL,
    skin        TINYINT NOT NULL,
    face        INT UNSIGNED NOT NULL,
    hair        INT UNSIGNED NOT NULL,
    -- TODO Should this be TINYINT?
    hair_color  INT UNSIGNED NOT NULL,

    -- TODO Is this map position or character index? If hts character index
    -- then move it up to name just for visual grouping when editing here. 1
    -- byte really is not enough for a map position so i have my doubts.
    pos         TINYINT UNSIGNED NOT NULL,
    map         INT UNSIGNED NOT NULL DEFAULT 0,

    PRIMARY KEY (id)
);

-- The entries with a null character id should be cleared on login server restarts
CREATE TABLE character_names (
    name            VARCHAR(12) NOT NULL UNIQUE,
    account_id      INT NOT NULL,
    character_id    INT DEFAULT NULL,

    PRIMARY KEY (name),
    FOREIGN KEY (account_id) REFERENCES accounts(id) ON DELETE CASCADE,
    FOREIGN KEY (character_id) REFERENCES characters(id) ON DELETE CASCADE
);

CREATE TABLE equips (
    character_id    INT NOT NULL,
    id              INT NOT NULL AUTO_INCREMENT,
    item_id         INT UNSIGNED NOT NULL,
    type            TINYINT NOT NULL,
    -- pos             SMALLINT NOT NULL,
    slots           TINYINT NOT NULL DEFAULT 7,
    scrolls         TINYINT NOT NULL DEFAULT 0,
    str             TINYINT NOT NULL DEFAULT 0,
    dex             TINYINT NOT NULL DEFAULT 0,
    -- int reserved :P
    intel           TINYINT NOT NULL DEFAULT 0,
    luk             TINYINT NOT NULL DEFAULT 0,
    hp              TINYINT NOT NULL DEFAULT 0,
    mp              TINYINT NOT NULL DEFAULT 0,
    watk            TINYINT NOT NULL DEFAULT 0,
    matk            TINYINT NOT NULL DEFAULT 0,
    wdef            TINYINT NOT NULL DEFAULT 0,
    mdef            TINYINT NOT NULL DEFAULT 0,
    acc             TINYINT NOT NULL DEFAULT 0,
    avo             TINYINT NOT NULL DEFAULT 0,
    hand            TINYINT NOT NULL DEFAULT 0,
    speed           TINYINT NOT NULL DEFAULT 0,
    jump            TINYINT NOT NULL DEFAULT 0,

    PRIMARY KEY (id),
    FOREIGN KEY (character_id) REFERENCES characters(id) ON DELETE CASCADE
);

CREATE TABLE items (
    character_id    INT NOT NULL,
    id              INT NOT NULL AUTO_INCREMENT,
    -- TODO Is this for equip/etc/use/cash/misc index?
    inv             TINYINT NOT NULL,
    -- TODO If this is for the item slot, i dont think there are more than 255
    -- slots... pretty sure 255 is max. Verify and change to TINYINT
    pos             SMALLINT NOT NULL,
    amount          SMALLINT NOT NULL,

    -- TODO This is likely going to be a composite key of character_id, id, inv, pos
    PRIMARY KEY (id),
    FOREIGN KEY (character_id) REFERENCES characters(id) ON DELETE CASCADE
);

-- TODO Go through the defaults in the garbage table from TitanMS and insert
-- them at character creation
CREATE TABLE keymap (
    character_id    INT NOT NULL,
    key_index       TINYINT NOT NULL,
    -- TODO Check the value range for this
    key_value       SMALLINT NOT NULL,

    PRIMARY KEY (character_id, key_index),
    FOREIGN KEY (character_id) REFERENCES characters(id) ON DELETE CASCADE
);

CREATE TABLE skills (
    character_id    INT NOT NULL,
    skill_id        INT NOT NULL,
    points          TINYINT NOT NULL,

    PRIMARY KEY (character_id, skill_id),
    FOREIGN KEY (character_id) REFERENCES characters(id) ON DELETE CASCADE
);

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSServerArgParser.h"

MSServerArgParser::MSServerArgParser()
{
    addOpt(ArgOpt('p', "port", REQUIRED_ARGUMENT, Variant((uint16_t)0),
            "Port to listen on"));
    // Parse with inet_aton
    addOpt(ArgOpt('i', "ip", REQUIRED_ARGUMENT, Variant(std::string()),
            "IP address to accept connections from"));
    addOpt(ArgOpt('l', "localhost", NO_ARGUMENT, Variant((bool)false),
            "Accept localhost only connections"));
    addOpt(ArgOpt('a', "any", NO_ARGUMENT, Variant((bool)false),
            "Accept connections from any IP address (default)"));
}

MSServerArgParser::~MSServerArgParser()
{
    // Pineapples
}

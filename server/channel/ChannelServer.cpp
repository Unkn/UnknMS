/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "ChannelServer.h"

ChannelServer::ChannelServer(
        std::unique_ptr<MSServerArgParser> && argParser,
        std::unique_ptr<MSPacketHandler> && packetHandler) :
    MSServer(std::move(argParser), std::move(packetHandler))
{
    // TODO Remove this shit
    port_ = 8888;
}

ChannelServer::~ChannelServer()
{
}

ChannelClient * ChannelServer::newClient(std::unique_ptr<Socket> && socket) const
{
    return new ChannelClient(std::move(socket));
}

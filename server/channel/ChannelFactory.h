/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CHANNELFACTORY_H
#define CHANNELFACTORY_H

#include <memory>

#include "Channel.h"

class ChannelFactory
{
    public:
        virtual std::unique_ptr<Channel> create() = 0;
};

#endif

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CHANNELSERVER_H
#define CHANNELSERVER_H

#include <memory>
#include <vector>

#include "MSServer.h"
#include "ChannelClient.h"

class ChannelServer : public MSServer
{
    public:
        ChannelServer() = delete;
        ChannelServer(std::unique_ptr<MSServerArgParser> && argParser, std::unique_ptr<MSPacketHandler> && packetHandler);
        virtual ~ChannelServer();

    protected:
        virtual ChannelClient * newClient(std::unique_ptr<Socket> && socket) const;

        // Move to world server
#if 0
    protected:
        virtual ArgParser * getArgParser();
    private:
        ChannelServerArgParser args_;
        std::vector<std::unique_ptr<Channel > > channels_;
        // TODO Inject it
        std::unique_ptr<ChannelFactory> channelFactory_;
#endif
};

#endif

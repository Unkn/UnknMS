/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "MSPPGetUserID.h"
#include "MSPacketHeaders.h"
#include "MSPacketBuilder.h"
#include "ChannelClient.h"

MSPPGetUserID::MSPPGetUserID()
{
}

MSPPGetUserID::~MSPPGetUserID()
{
}

uint16_t MSPPGetUserID::getHeader() const
{
    return MSPacketHeaders::v55::CONNECT_CHANNEL;
}

bool MSPPGetUserID::process(
        Buffer &buffIn,
        std::vector<Buffer> & outBuffs,
        MapleClient &client __attribute__ ((unused)))
{
    // 14 00 header
    // 01 00 00 00 id
    // 00 00 ??
    bool ret = true;
    uint16_t header;
    uint32_t id;

    Packet p(buffIn);
    p.get(header);
    p.get(id);

    // TODO validation

    // TODO Interface to get cahracters from...
    // Use a dummy character for now (default new char which whatever stats)
    MSCharacter c;

    c.level_ = 1;
    c.gender_ = MSGender::Male; // male
    c.skin_ = 0x00;
    c.pos_ = 0;
    c.job_ = MSJob::Beginner;
    c.str_ = 123;
    c.dex_ = 321;
    c.int_ = 213;
    c.luk_ = 312;
    c.curHP_ = 1000;
    c.maxHP_ = 2000;
    c.curMP_ = 3000;
    c.maxMP_ = 4000;
    c.ap_ = 0;
    c.sp_ = 0;
    c.fame_ = 1337;
    c.face_ = 0x00004E20;
    c.hair_ = 0x0000754E;
    c.dbid_ = 1;
    c.exp_ = 1;
    c.map_ = 0x00000000;
    c.name_ = "fuck";
    c.equips_ = std::vector<MSEquip>
    {
        { 0, MSEquipType::Coat, 0x000FDE82 },
        { 0, MSEquipType::Pants, 0x00102CA2 },
        { 0, MSEquipType::Shoes, 0x00105B81 },
        { 0, MSEquipType::Weapon, 0x0013DDF0 }
    };

    ret = addConnectData(outBuffs, c) /*&& the other packet*/;

    return ret;
}

bool MSPPGetUserID::addConnectData(std::vector<Buffer> & outBuffs, MSCharacter & c)
{
    bool ret = true;
    MSPacketBuilder b;

    b.add<uint16_t>(0x4E); // header
    b.add<uint32_t>(0); // channel id

    // TODO What is this :|
    std::vector<uint8_t> wtf
    {
        0x01, 0x01, 0x85, 0x3D, 0x4B, 0x11, 0xF4, 0x83, 0x6B, 0x3D, 0xBA, 0x9A, 0x4F, 0xA1,
    };
    b.add(wtf.data(), wtf.size());

    b.add<uint16_t>(0xFF); // TODO ?

    b.add(c);

    b.add<uint8_t>(0x14); // TODO ?
    b.add<uint32_t>(123456); // mesos
    // TODO Probably slot count? Vary it up to verify
    for (int i = 0; i < 5; ++i)
        b.add<uint8_t>(100);

    // TODO Add the equip section of packet. There is a loop for non-cash then
    // cash equips for equip pos <0 separated by a null byte and then a list of
    // all equips with pos >0 after another null nyte
    b.add<uint8_t>(0); // ^ that separator
    b.add<uint8_t>(0); // ^ and the next one

    // TODO Skills

    // end... so titan says
    for (int i = 0; i < 3; ++i)
        b.add<uint32_t>(0); // TODO ?
    b.add<uint16_t>(0); // TODO ?
    for(int i = 0; i < 15; ++i)
        b.add<uint32_t>(0x3B9AC9FF); // TODO ?

    // TODO Server time... maybe this garbage will get us by... for the testing
    // period. Should see if we can get a std::chrono::system_time::now() to
    // FILETIME or whatever the windows struct is
    uint64_t epochDiff = 116444736000000000LL;
    uint64_t francTime = 1441134000LL;
    uint64_t created = francTime * 1000000 + epochDiff;
    b.add<uint64_t>(created); 

    if (ret)
        outBuffs.emplace_back(std::move(b.getRaw()));

    return ret;
}

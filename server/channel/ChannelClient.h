/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CHANNELCLIENT_H
#define CHANNELCLIENT_H

#include "MapleClient.h"

class ChannelClient : public MapleClient
{
    public:
        ChannelClient(std::unique_ptr<Socket> && socket);
        virtual ~ChannelClient();
};

#endif

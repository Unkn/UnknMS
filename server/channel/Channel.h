/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef CHANNEL_H
#define CHANNEL_H

#include <memory>
#include <cstdint>

#include "Socket.h"

class Channel
{
    public:
        using ID = int8_t;

        Channel(ID id, std::unique_ptr<Socket> && socket);
        virtual ~Channel();

    private:
        const ID id_;
        std::unique_ptr<Socket> socket_;
};

#endif

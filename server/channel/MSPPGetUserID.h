/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSPPGETUSERID_H
#define MSPPGETUSERID_H

#include "MSPacketProcessor.h"
#include "MSCharacter.h"

// TODO Rename this to like entering channel request or somethin?
class MSPPGetUserID : public MSPacketProcessor
{
    public:
        MSPPGetUserID();
        virtual ~MSPPGetUserID();

        virtual uint16_t getHeader() const;
        virtual bool process(Buffer &buffIn, std::vector<Buffer> & outBuffs, MapleClient &client);

    private:
        bool addConnectData(std::vector<Buffer> & outBuffs, MSCharacter &client);
};

// TODO Is this where it should go?
namespace std
{
    unique_ptr(MSPPGetUserID *) -> unique_ptr<MSPPGetUserID, default_delete<MSPPGetUserID> >;
}

#endif

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "Channel.h"

Channel::Channel(ID id, std::unique_ptr<Socket> && socket) :
    id_(id),
    socket_(std::move(socket))
{
}

Channel::~Channel()
{
}

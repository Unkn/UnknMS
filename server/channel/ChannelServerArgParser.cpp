/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "ChannelServerArgParser.h"
#include "Channel.h"

ChannelServerArgParser::ChannelServerArgParser()
{
    addOpt(ArgOpt('c', "channels", REQUIRED_ARGUMENT, Variant(Channel::ID(1)),
            "Total channels to for the channel server to run"));
}

ChannelServerArgParser::~ChannelServerArgParser()
{
    // Pineapples
}

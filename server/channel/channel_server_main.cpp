/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "StdOutLogMethod.h"
#include "WebSocketLogMethod.h"
#include "Logger.h"

#include "ChannelServer.h"
#include "MSPPGetUserID.h"

int main(int argc, char **argv)
{
    USE_STDOUT_LOG_METHOD;

    auto wslm = std::make_unique<WebSocketLogMethod>();
    wslm->port_ = 10000;
    wslm->addr_ = "localhost";
    Logger::addLogMethod(std::move(wslm));
    Logger::setLogID("channel");

    // TODO Start throwing in packet processing
    auto packetHandler = std::make_unique<MSPacketHandler>();

    packetHandler->registerProcessor(std::make_unique<MSPPGetUserID>());

    ChannelServer channelServer(
            std::move(std::make_unique<MSServerArgParser>()),
            std::move(packetHandler));

    return channelServer.main(argc, argv);
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "ChannelClient.h"

ChannelClient::ChannelClient(std::unique_ptr<Socket> && socket) :
    MapleClient(std::move(socket))
{
}

ChannelClient::~ChannelClient()
{
    // Pineapples
}

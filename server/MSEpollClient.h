/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSEPOLLCLIENT_H
#define MSEPOLLCLIENT_H

#include <memory>

#include "MapleClient.h"

// Rename to somethin shorter?
struct MSEpollClient
{
    MSEpollClient() :
        fd_(-1),
        client_()
    {
    }

    MSEpollClient(const MSEpollClient & obj) = delete;

    MSEpollClient(MSEpollClient && obj)
    {
        *this = std::move(obj);
    }

    MSEpollClient(int fd, std::unique_ptr<MapleClient> && client) :
        fd_(fd),
        client_(std::move(client))
    {
    }

    MSEpollClient & operator=(const MSEpollClient & rhs) = delete;

    MSEpollClient & operator=(MSEpollClient && rhs)
    {
        if (this != &rhs)
        {
            fd_ = rhs.fd_;
            client_ = std::move(rhs.client_);
        }
        return *this;
    }

    int fd_;
    std::unique_ptr<MapleClient> client_;
    // TODO KILL_INACTIVE Timestamp for last active. We are going to want to
    // have a way to ping the clients at character creation because the entire
    // rolling process appears to be client sided.
};

#endif

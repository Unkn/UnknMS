/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MSSERVER_H
#define MSSERVER_H

#include <memory>
#include <set>

#include "Program.h"
#include "MSPacketHandler.h"
#include "MSServerArgParser.h"
#include "MSEpollClient.h"

class MSServer : public Program
{
    public:
        MSServer() = delete;
        MSServer(std::unique_ptr<MSServerArgParser> && argParser, std::unique_ptr<MSPacketHandler> && packetHandler);
        virtual ~MSServer();

    protected:
        virtual int run();
        virtual MSServerArgParser * getArgParser();

        virtual MapleClient * newClient(std::unique_ptr<Socket> && socket) const = 0;
        virtual void disconnect_client_cleanup(MapleClient & client);

    private:
        int setupListeningSocket();
        int setupEpoll(int listenFD);
#ifdef SERVER_SIGINT
        bool setupSIGINTHandler(int epollFD);
#endif
        // TODO Probably just make these FD's members... maybe
        bool setNonBlocking(int fd) const;
        // TODO Rename this...
        bool doShit(int listenFD, int epollFD);

        std::unique_ptr<MSServerArgParser> argParser_;
        std::unique_ptr<MSPacketHandler> packetHandler_;
        // TODO KILL_INACTIVE Use like a max heap or somethin so that all the
        // clients with the longest inactivity are on the top so we can easily
        // kill off inactive connections.
        //std::set<std::unique_ptr<MSEpollClient> > clients_;

    // TODO Remove this and finish the arg parsing....
    public:
        uint16_t port_;
};

#endif

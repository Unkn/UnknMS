/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#include "Logger.h"
#include "MSServer.h"
#include "LinuxSocket.h"

#ifdef SERVER_SIGINT
// TODO Come up with a cleaner way to do this
#include <sys/eventfd.h>
#include <csignal>

static int sigintFD = -1;

static void signalHandler(int signal __attribute__ ((unused)))
{
    // TODO Switch based on signal?
    LOG_NOTICE("Handling SIGINT");
    eventfd_write(sigintFD, 1);
}
#endif

MSServer::MSServer(
        std::unique_ptr<MSServerArgParser> && argParser __attribute__ ((unused)),
        std::unique_ptr<MSPacketHandler> && packetHandler) :
    // TODO fixit
    argParser_(nullptr/*std::move(argParser)*/),
    packetHandler_(std::move(packetHandler))
{
}

MSServer::~MSServer()
{
}

int MSServer::run()
{
    bool ok = true;

    int epollFD = -1;
    int listenFD = setupListeningSocket();
    if (listenFD < 0)
    {
        LOG_CRIT("Failed to setup listening socket");
        ok = false;
    }
    else if ((epollFD = setupEpoll(listenFD)) < 0)
    {
        LOG_CRIT("Failed to setup epoll");
        ok = false;
        close(listenFD);
        listenFD = -1;
    }

    if (ok)
    {
        // TODO Just for me :| ... get rid of this
        int rounds = 0;
        do
        {
            LOG_DEBUG("{} {:d}", std::string(128, '='), rounds++);
        } while (doShit(listenFD, epollFD));

        LOG_NOTICE("Server shutting down");
    }

#ifdef SERVER_SIGINT
    if (sigintFD >= 0)
        close(sigintFD);
#endif
    if (listenFD >= 0)
        close(listenFD);
    if (epollFD >= 0)
        close(epollFD);

    return ok ? 0 : -1;
}

MSServerArgParser * MSServer::getArgParser()
{
    return argParser_.get();
}

void MSServer::disconnect_client_cleanup(MapleClient & client __attribute__ ((unused)))
{
    // Do nothing by default
}

int MSServer::setupListeningSocket()
{
    bool ok = false;
    int listenFD = -1;
    struct sockaddr_in listenAddr;

    memset(&listenAddr, 0, sizeof(listenAddr));

    listenAddr.sin_family = AF_INET;
    // TODO Option for how to bind
    //listenAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    listenAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    // TODO Option to specify port
    listenAddr.sin_port = htons(port_);

    if ((listenFD = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        LOG_CRIT("Failed to create connection accept socket");
    else if (bind(listenFD, (struct sockaddr *)&listenAddr, sizeof(listenAddr)) < 0)
        LOG_CRIT("Failed to bind connection accept socket");
    // TODO Option for backlog
    else if (listen(listenFD, 10) < 0)
        LOG_CRIT("Failed to listen on the accept socket");
    else
        ok = true;

    if (!ok && listenFD >= 0)
    {
        close(listenFD);
        listenFD = -1;
    }

    return listenFD;
}

int MSServer::setupEpoll(int listenFD)
{
    struct epoll_event ev;
    int epollFD = -1;

    ev.events = EPOLLIN;
    ev.data.fd = listenFD;

    // TODO epoll is linux specific. Will need to abtract some API for
    // dealing with connection acception and management between windows and
    // linux
    if ((epollFD = epoll_create1(0)) < 0)
    {
        LOG_CRIT("Failed to create epoll fd");
    }
    else if (epoll_ctl(epollFD, EPOLL_CTL_ADD, listenFD, &ev) < 0)
    {
        LOG_CRIT("Failed to add listening socket to epoll");
        close(epollFD);
        epollFD = -1;
    }
#ifdef SERVER_SIGINT
    else
    {
        MSServer::setupSIGINTHandler(epollFD);
    }
#endif

    return epollFD;
}

#ifdef SERVER_SIGINT
bool MSServer::setupSIGINTHandler(int epollFD)
{
    bool ret = false;

    if (sigintFD >= 0)
    {
        LOG_WARN("Something else already initialized the SIGINT eventfd");
    }
    else if ((sigintFD = eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK)) < 0)
    {
        LOG_WARN("Failed to create SIGINT event fd");
    }

    if (sigintFD >= 0)
    {
        struct epoll_event ev;
        ev.events = EPOLLIN | EPOLLET;
        ev.data.fd = sigintFD;

        if (epoll_ctl(epollFD, EPOLL_CTL_ADD, sigintFD, &ev) < 0)
        {
            LOG_CRIT("Failed to add SIGINT eventfd to epoll set");
        }
        else
        {
            LOG_INFO("Registered SIGINT handler");
            std::signal(SIGINT, signalHandler);
            ret = true;
        }
    }

    if (!ret)
        LOG_NOTICE("Failed to registered SIGINT handler");

    return ret;
}
#endif

bool MSServer::setNonBlocking(int fd) const
{
    bool ok = true;
    int flags;

    if ((flags = fcntl(fd, F_GETFL, 0)) < 0)
    {
        LOG_ERROR("Failed to get access flags for socket {:d} ({:d})", fd, errno);
        ok = false;
    }
    else if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) < 0)
    {
        LOG_ERROR("Failed to get set non-blocking flag for socket {:d} ({;d})", fd, errno);
        ok = false;
    }

    return ok;
}

bool MSServer::doShit(int listenFD, int epollFD)
{
    // TODO config option?
    const int maxEpollEvents = 10;
    struct epoll_event ev;
    // TODO malloc this if sufficiently large
    struct epoll_event events[maxEpollEvents];
    int nfds;

    LOG_INFO("Waiting for new client connections");

    // TODO KILL_INACTIVE Have a finite timeout so we can check for inactivity
    // in some interval if there are any connected clients.
    nfds = epoll_wait(epollFD, events, maxEpollEvents, -1);
    if (nfds < 0)
    {
        LOG_WARN("epoll_wait call failure");
    }

    for (int i = 0; i < nfds; ++i)
    {
#ifdef SERVER_SIGINT
        if (events[i].data.fd == sigintFD)
        {
            LOG_NOTICE("Processing SIGINT induced server exit");
            return false;
        }
        else
#endif
        // TODO The code from the epoll example in man pages puts the
        // listening socket in the epoll set with all the connections.
        // We may want to separate this out so the connection accepting
        // thread is more or less not interrupted by active clients.
        //
        // We kind of NEED to separate it anyways because the other
        // connections we used the void * instead of the fd so the
        // comparison check isn't REALLY safe... but considering valid
        // address ranges and valid fd ranges... i guess it might be :|
        if (events[i].data.fd == listenFD)
        {
            // TODO See accept man page about the sockaddr param
            int newConnFD = accept(listenFD, nullptr, nullptr);
            if (newConnFD < 0)
            {
                LOG_WARN("Error acception new client connection");
                continue;
            }
            else if (!setNonBlocking(newConnFD))
            {
                LOG_WARN("Error setting new client connection to non-blocking");
                continue;
            }

            // TODO Factory?
            auto clientSocket = std::unique_ptr(static_cast<Socket *>(new LinuxSocket(newConnFD)));
            auto ms_client = std::unique_ptr(newClient(std::move(clientSocket)));
            auto epoll_client = std::make_unique<MSEpollClient>(newConnFD, std::move(ms_client));

            ev.data.ptr = epoll_client.get();
            ev.events = EPOLLIN | EPOLLET;

            if (!epoll_client->client_->establishConnection())
                LOG_ERROR("Failed to establish connection with new client {:d}", newConnFD);
            else if (epoll_ctl(epollFD, EPOLL_CTL_ADD, newConnFD, &ev) < 0)
                LOG_ERROR("Failed to add new connection to epoll set");
            else
                // TODO KILL_INACTIVE Add client to max heap
                epoll_client.release();
        }
        else
        {
            auto epoll_client = static_cast<MSEpollClient *>(events[i].data.ptr);
            auto & ms_client = *epoll_client->client_;

            // TODO Send the client to a worker thread for processing
            // TODO Kick clients with rejected packets
            if (!packetHandler_->handle(ms_client))
            {
#ifdef DISCONNECT_UNHANDLED_PACKETS
                bool disconnect = true;
                LOG_WARN("Failed to handle packet from client {:d}, disconnecting", ms_client.getConnectionID());
#else
                bool disconnect = ms_client.should_disconnect();
                LOG_WARN("Failed to handle packet from client {:d}", ms_client.getConnectionID());
#endif
                if (disconnect)
                {
                    if (epoll_ctl(epollFD, EPOLL_CTL_DEL, epoll_client->fd_, nullptr) < 0)
                        LOG_WARN("Error removing client socket from epoll set: {}", strerror(errno));
                    disconnect_client_cleanup(ms_client);
                    delete epoll_client;
                }
            }
        }
    }

    // TODO KILL_INACTIVE Check for clients that have been idle for too long to kick

    return true;
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "StdOutLogMethod.h"
#include "WebSocketLogMethod.h"
#include "Logger.h"

#include "HSMConnection.h"
#include "LinuxSocketFactory.h"

#include "LoginServer.h"
#include "MSPacketHandler.h"
#include "MSPPLoginUser.h"
#include "MSHandleLoginPP.h"
#include "MSRegisterPINPP.h"
#include "MSPPShowWorlds.h"
#include "MSPPSelectWorld.h"
#include "MSPPSelectChannel.h"
#include "MSPPConnectGame.h"
#include "MSPPCheckName.h"
#include "MSPPCreateCharacter.h"
#include "MSWorld.h"
#include "MSServerArgParser.h"

#include "MSDBAccount.h"
#include "MSDBCharacter.h"

int main(int argc, char **argv)
{
    USE_STDOUT_LOG_METHOD;

    auto wslm = std::make_unique<WebSocketLogMethod>();
    wslm->port_ = 10000;
    wslm->addr_ = "localhost";
    Logger::addLogMethod(std::move(wslm));
    Logger::setLogID("login");

    auto hsmsocket = LinuxSocketFactory::connect_localhost(9999);
    if (!hsmsocket)
    {
        LOG_ERROR("Failed to connect to toy hsm service");
        return -1;
    }
    auto hsmconn = std::make_shared<HSMConnection>(std::move(hsmsocket));

    auto dbconn = std::make_shared<MySQLConnection>();
    if (!dbconn->connect("localhost", "root", "fuck", "login_server"))
    {
        LOG_ERROR("Failed to connect to login_server db");
        return -1;
    }
    // TODO It might make more sense to give the dbconn to the packet processor
    // and let them construct the db accessors they need. However it wouldn't
    // be as easily unit testable or restricted.
    auto msdb_account = std::make_shared<MSDBAccount>(dbconn);
    auto msdb_char = std::make_shared<MSDBCharacter>(dbconn);

    // In case there are lingering unused names in the db, we want to clear
    // those so they are available for use before starting up the server.
    msdb_char->clear_all_unused_names();

    auto packetHandler = std::make_unique<MSPacketHandler>();

    packetHandler->registerProcessor(std::make_unique<MSPPLoginUser>(hsmconn, msdb_account));
    packetHandler->registerProcessor(std::make_unique<MSHandleLoginPP>());
    packetHandler->registerProcessor(std::make_unique<MSRegisterPINPP>());

    auto worlds = std::make_shared<std::vector<MSWorld> >();
    worlds->emplace_back(0, 1, 0, "Scania");
    packetHandler->registerProcessor(std::make_unique<MSPPShowWorlds>(worlds));

    packetHandler->registerProcessor(std::make_unique<MSPPSelectWorld>());
    packetHandler->registerProcessor(std::make_unique<MSPPSelectChannel>(msdb_char));
    packetHandler->registerProcessor(std::make_unique<MSPPConnectGame>());
    packetHandler->registerProcessor(std::make_unique<MSPPCheckName>(msdb_char));
    packetHandler->registerProcessor(std::make_unique<MSPPCreateCharacter>(msdb_char));

    LoginServer loginServer(
            std::move(std::make_unique<MSServerArgParser>()),
            std::move(packetHandler),
            msdb_char);

    return loginServer.main(argc, argv);
}


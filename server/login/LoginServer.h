/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef LOGINSERVER_H
#define LOGINSERVER_H

#include "MSServer.h"
#include "MSLoginClient.h"
#include "MSDBCharacter.h"

class LoginServer : public MSServer
{
    public:
        LoginServer() = delete;
        LoginServer(std::unique_ptr<MSServerArgParser> && argParser, std::unique_ptr<MSPacketHandler> && packetHandler, const std::shared_ptr<MSDBCharacter> & msdb_char);
        virtual ~LoginServer();

    protected:
        virtual MSLoginClient * newClient(std::unique_ptr<Socket> && socket) const;
        virtual void disconnect_client_cleanup(MapleClient & client);

    private:
        std::shared_ptr<MSDBCharacter> msdb_char_;
};

#endif

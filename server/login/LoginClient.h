/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef LOGINCLIENT_H
#define LOGINCLIENT_H

#include <memory>

#include "MapleClient.h"
#include "MSCrypt.h"
#include "Socket.h"
#include "buffer.hpp"

class LoginClient : public MapleClient
{
    public:
        LoginClient() = delete;
        LoginClient(std::unique_ptr<Socket> && socket);

        virtual bool establishConnection();
        virtual bool readPacket(Buffer &buff);
        virtual bool sendPacket(const Buffer &buff);

        int getConnectionID() const;

    private:
        std::unique_ptr<Socket> socket_;
        uint8_t sendIV_[MSCrypt::IV_LENGTH];
        uint8_t recvIV_[MSCrypt::IV_LENGTH];
};

#endif

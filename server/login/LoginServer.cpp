/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "LoginServer.h"
#include "MSLoginClient.h"

LoginServer::LoginServer(
        std::unique_ptr<MSServerArgParser> && argParser,
        std::unique_ptr<MSPacketHandler> && packetHandler,
        const std::shared_ptr<MSDBCharacter> & msdb_char) :
    MSServer(std::move(argParser), std::move(packetHandler)),
    msdb_char_(msdb_char)
{
    // TODO Remove this shit
    port_ = 8484;
}

LoginServer::~LoginServer()
{
}

MSLoginClient * LoginServer::newClient(std::unique_ptr<Socket> && socket) const
{
    return new MSLoginClient(std::move(socket));
}

void LoginServer::disconnect_client_cleanup(MapleClient & client)
{
    MSServer::disconnect_client_cleanup(client);
    if (!client.reserved_name_.empty())
    {
        LOG_DEBUG("Releasing unused named reserved for account {:d}", client.account_->id_);
        msdb_char_->release_unused_names(client.account_->id_);
    }
}

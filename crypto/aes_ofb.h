/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef AESOFB_H
#define AESOFB_H

#ifdef __cplusplus
extern "C" {
#endif

// TODO Test openssl speed compared to this to see if its work linking openssl
int aes256_ofb_encrypt(unsigned char* plaintext, int plaintext_len, const unsigned char* key, unsigned char* iv, unsigned char *ciphertext);
int aes256_ofb_decrypt(unsigned char* ciphertext, int ciphertext_len, const unsigned char* key, unsigned char* iv, unsigned char *plaintext);

#ifdef __cplusplus
}
#endif

#endif

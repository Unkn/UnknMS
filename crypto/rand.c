/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <unistd.h>
#include <stdio.h>
// One of these is for open...
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "rand.h"

int generateRandomBytes(void * bytes, int len)
{
    // TODO This is platform specific. Add a windows variant when the time comes.
    ssize_t ret = -1;
    int fd;
    if (!bytes || len <= 0)
    {
        fprintf(stderr, "%s - invalid args\n", __func__);
    }
    else if ((fd = open("/dev/urandom", O_RDONLY)) < 0)
    {
        perror("open - /dev/urandom");
    }
    else if ((ret = read(fd, bytes, len)) < 0)
    {
        perror("read - /dev/urandom");
        ret = -1;
    }
    if (fd >= 0)
        close(fd);
    return ret;
}

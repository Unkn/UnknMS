/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <openssl/evp.h>

static void checkOpenSSLInit()
{
    // TODO This needs to be done differently probably for flexibility
    static int opensslInit = 0;
    if (!opensslInit)
    {
        opensslInit = 1;
        // Look into initializing only what is needed
        OpenSSL_add_all_algorithms();
    }
}

#ifdef CRYPTO_DEBUG
#include <string.h>
void pbuff(const char * id, const unsigned char * data, size_t len)
{
    int idlen = strlen(id);
    int i;
    printf("%s", id);
    for (i = idlen; i < 20; ++i)
        printf(" ");
    for (i = 0; i < len; ++i)
        printf("%02X ", data[i]);
    printf("\n");
}
#endif

int aes256_ofb_encrypt(
        unsigned char* plaintext,
        int plaintext_len,
        const unsigned char* key,
        unsigned char* iv,
        unsigned char *ciphertext)
{
#ifdef CRYPTO_DEBUG
    printf("%s\n", __func__);
    pbuff("plaintext", plaintext, plaintext_len);
    pbuff("key", key, 32);
    pbuff("iv", iv, 16);
#endif

    checkOpenSSLInit();

    EVP_CIPHER_CTX *ctx;
    int len;
    int ciphertext_len;

    // This is gonna be bad, just need to see it work first...
    if (!(ctx = EVP_CIPHER_CTX_new()))
        fprintf(stderr, "%s", "EVP_CIPHER_CTX_new");

    if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ofb(), 0, key, iv))
        fprintf(stderr, "%s", "EVP_EncryptInit_ex");

    if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        fprintf(stderr, "%s", "EVP_EncryptUpdate");

    ciphertext_len = len;

    if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        fprintf(stderr, "%s", "EVP_EncryptFinal_ex");

    ciphertext_len += len;

    EVP_CIPHER_CTX_free(ctx);

#ifdef CRYPTO_DEBUG
    pbuff("ciphertext", ciphertext, ciphertext_len);
#endif

    return ciphertext_len;
}

int aes256_ofb_decrypt(
        unsigned char* ciphertext,
        int ciphertext_len,
        const unsigned char* key,
        unsigned char* iv,
        unsigned char *plaintext)
{
    checkOpenSSLInit();

#ifdef CRYPTO_DEBUG
    printf("%s\n", __func__);
    pbuff("ciphertext", ciphertext, ciphertext_len);
    pbuff("key", key, 32);
    pbuff("iv", iv, 16);
#endif

    EVP_CIPHER_CTX *ctx;
    int len;
    int plaintext_len;

    // This is gonna be bad, just need to see it work first...
    if (!(ctx = EVP_CIPHER_CTX_new()))
        fprintf(stderr, "%s", "EVP_CIPHER_CTX_new");

    if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_ofb(), 0, key, iv))
        fprintf(stderr, "%s", "EVP_DecryptInit_ex");

    if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
        fprintf(stderr, "%s", "EVP_DecryptUpdate");

    plaintext_len = len;

    if (1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len))
        fprintf(stderr, "%s", "EVP_DecryptFinal_ex");

    plaintext_len += len;

    EVP_CIPHER_CTX_free(ctx);

#ifdef CRYPTO_DEBUG
    pbuff("plaintext", plaintext, plaintext_len);
#endif

    return plaintext_len;
}

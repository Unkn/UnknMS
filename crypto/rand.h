/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef RAND_H
#define RAND_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h> // size_t

int generateRandomBytes(void * bytes, int len);

#ifdef __cplusplus
}
#endif

#endif
